# Creating & Uploading Packages

## Python Package Index (for `pip`)

1) In project root directory:
    ```python
    python setup.py check
    python setup.py sdist bdist_wheel
    ```

    This creates the package archives in `./dist`.

2) Upload to PyPI:
    ```python
        twine upload dist/*
    ```


## Anaconda (for `conda`)

3) go to `./conda-build` and do 
    ```python
    conda build purge
    conda build . --prefix-length=200
    ``` 

4) Be logged in using `anaconda` and do
    ```python
    anaconda upload --user LieTorch <home>/miniconda3/conda-bld/noarch/lietorch-<version>.tar.bz2
    ```