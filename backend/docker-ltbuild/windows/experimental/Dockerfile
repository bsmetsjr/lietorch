# escape=`

#
#   Container for building ML packages such as PyTorch or LieTorch.
#
#

FROM mcr.microsoft.com/windows/servercore:20H2

LABEL maintainer="b.m.n.smets@tue.nl"
LABEL version="experimental"
LABEL platform="windows"


# Reset the shell.
SHELL ["cmd", "/S", "/C"]


# Download and install Build Tools for Visual Studio 2019 (v16.10.4).
ARG VS_BUILDTOOLS=https://download.visualstudio.microsoft.com/download/pr/acfc792d-506b-4868-9924-aeedc61ae654/72ae7ec0c234bbe0e655dc4776110c23178c8fbb7bbcf9b5b96a683b95e8d755/vs_BuildTools.exe
ADD ${VS_BUILDTOOLS} C:\TEMP\vs_buildtools.exe
RUN C:\TEMP\vs_buildtools.exe --quiet --wait --norestart --nocache `
    --add Microsoft.VisualStudio.Workload.VCTools --includeRecommended`
    --installPath C:\BuildTools  `
    || IF "%ERRORLEVEL%" == "3010" EXIT 0


# Download and install CUDA Toolkit (v11.1)
ARG CUDA_TOOLKIT=https://developer.download.nvidia.com/compute/cuda/11.1.1/network_installers/cuda_11.1.1_win10_network.exe
ADD ${CUDA_TOOLKIT} C:\TEMP\cuda_toolkit.exe
RUN C:\TEMP\cuda_toolkit.exe -s nvcc_11.1 cudart_11.1 && setx /M PATH "%PATH%;C:\Program Files\NVIDIA GPU COMPUTING Toolkit\CUDA\v11.1\bin"


# Download and install Git for Windows (v2.32)
ARG GIT_SETUP=https://github.com/git-for-windows/git/releases/download/v2.32.0.windows.2/Git-2.32.0.2-64-bit.exe
ADD ${GIT_SETUP} C:\TEMP\git-setup.exe
RUN C:\TEMP\git-setup.exe /VERYSILENT /NORESTART /NOCANCEL /SP- /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS /COMPONENTS="icons,ext\reg\shellhere,assoc,assoc_sh"


# Download and unzip LibTorch (v1.9.1)
ARG LIBTORCH_Release=https://download.pytorch.org/libtorch/cu111/libtorch-win-shared-with-deps-1.9.1%2Bcu111.zip
ARG LIBTORCH_Debug=https://download.pytorch.org/libtorch/cu111/libtorch-win-shared-with-deps-debug-1.9.1%2Bcu111.zip
ADD ${LIBTORCH_Release} C:\TEMP\libtorch-release.zip
#ADD ${LIBTORCH_Debug} C:\TEMP\libtorch-debug.zip
RUN powershell.exe -Command $ProgressPreference = 'SilentlyContinue'; `
    $ProgressPreference = 'Continue'; `
    Expand-Archive -Path C:\TEMP\libtorch-release.zip -DestinationPath C:\ -Force; `
    #Expand-Archive -Path C:\TEMP\libtorch-debug.zip -DestinationPath C:\TEMP\ -Force; `
    #Rename-Item -Path C:\TEMP\libtorch -NewName libtorch-debug; `
    #Move-Item -Path C:\TEMP\libtorch-debug -Destination C:\ -Force; `
    setx /M TORCH_INSTALL_HINT 'C:'; `
    Remove-Item C:\TEMP\* -Recurse -Force

# Patch for an nvcc/msvc issue
COPY tracer.h C:\libtorch\include\torch\csrc\jit\frontend\
COPY tracer.h C:\libtorch-debug\include\torch\csrc\jit\frontend\


# Patch DNS servers, adding a VeriSign and L3 server
COPY startup.ps1 C:\startup.ps1

WORKDIR C:\SRC
ENTRYPOINT ["C:\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "-arch=amd64", "-host_arch=amd64", "&&", "powershell.exe", "-File", "C:\\startup.ps1", "&&"]
CMD ["powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]