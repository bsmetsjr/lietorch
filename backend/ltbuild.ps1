<#
.SYNOPSIS
    Powershell script for running the Docker container that will compile the native backend for Windows.

.PARAMETER BuildType
    Specifies the type of build, must be one of Release, RelWithDebInfo, Debug or MinRelSize.

.PARAMETER Parallel
    Specifies the amount of concurrent build jobs to execute, defaults to 16.

#>
param ($BuildType = "Release", $Parallel = 4)

$ProjectPath = (Get-Item $PSScriptRoot).parent.FullName

# Check if the BuildType is allowed
$BuildTypes = @("Release", "RelWithDebInfo", "Debug", "MinRelSize")
if ($BuildTypes -NotContains $BuildType) {
    Write-Host -Separator "" -ForegroundColor Red `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] BuildType must be Release, RelWithDebInfo, Debug or MinRelSize.")
    Exit 1
}

$PrefixPath = Invoke-Command -ScriptBlock { python -c 'import torch;print(torch.utils.cmake_prefix_path)' }

cmake -DCMAKE_BUILD_TYPE="$BuildType" `
    -DCMAKE_PREFIX_PATH="$PrefixPath" `
    -B"$ProjectPath\backend\build" `
    -S"$ProjectPath\backend" `
    -GNinja

cmake --build "$ProjectPath\backend\build" `
    --target all --config "$BuildType" -j $Parallel

Write-Host -Separator "" -ForegroundColor Green `
    ("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Copy lietorch.dll to \lietorch\lib\")
New-Item -ItemType Directory -Force -Path "$ProjectPath\lietorch\lib"
Copy-Item -Path "$ProjectPath\backend\build\lietorch.dll" -Destination "$ProjectPath\lietorch\lib\" -Force

Write-Host -Separator "" -ForegroundColor Green `
("[ltbuild.ps1 ", (Get-Date -Format HH:mm:ss) ,"] Done.")
Exit 0

