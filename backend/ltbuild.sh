#!/usr/bin/env bash
set -e
set -o pipefail

SCRIPT_PATH="$( dirname -- "$( readlink -f -- "$0"; )"; )"

cmake -DCMAKE_BUILD_TYPE=Release \
    -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda/ \
    -DCMAKE_CUDA_COMPILER=/usr/local/cuda/bin/nvcc \
    -DCMAKE_PREFIX_PATH=`python -c 'import torch;print(torch.utils.cmake_prefix_path)'` \
    -B"$SCRIPT_PATH/build" \
    -S"$SCRIPT_PATH" \
    -GNinja

cmake --build "$SCRIPT_PATH/build" --target all -j4

mkdir -p $SCRIPT_PATH/../lietorch/lib/
cp $SCRIPT_PATH/build/liblietorch.so $SCRIPT_PATH/../lietorch/lib/liblietorch.so