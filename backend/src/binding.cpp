/*
    This file contains the TorchScript-compatible interface for the C++/CUDA
   backend. The most important part is the register where functions are
   registered with the PyTorch/TorchScript runtime.
*/

#include "torch_include.h"

#if !defined(__INTELLISENSE__)
#include <gitversion.h>
#else
#include "gitversion.h.in"
#endif

#include "generic.h"
#include "m2.h"
#include "r2.h"
#include "temperate.h"

#ifdef WITH_CUDA
#include <cuda.h>
#endif

namespace lietorch
{

/*
    Helper function that returns what CUDA version lietorch was built with.
    This needs to be the same version as PyTorch was built with.
*/
constexpr int64_t
_cuda_version()
{
#ifdef WITH_CUDA
    return CUDA_VERSION;
#else
    return -1;
#endif
}

/*
    Return build version string, obtained from `git describe --dirty`.
*/
std::string
_build_version()
{
    return std::string(GIT_BUILD_STRING);
}

/*
    Return build timestamp string, filled in by CMake at configuration time.
*/
std::string
_build_timestamp()
{
    return std::string(BUILD_TIMESTAMP_STRING);
}

} // namespace lietorch

/*

*/

/* Expose library interface  */
TORCH_LIBRARY(lietorch, m)
{
    m.def("_cuda_version", ::lietorch::_cuda_version);
    m.def("_build_version", ::lietorch::_build_version);
    m.def("_build_timestamp", ::lietorch::_build_timestamp);

    m.def("generic_add_fw", ::lietorch::generic::add_fw);
    m.def("generic_add_bw", ::lietorch::generic::add_bw);

    m.def(
        "generic_grayscale_dilation_2d",
        ::lietorch::generic::grayscale_dilation_2d);
    m.def(
        "generic_grayscale_erosion_2d",
        ::lietorch::generic::grayscale_erosion_2d);

    m.def(
        "m2_anisotropic_dilated_project",
        ::lietorch::m2::anisotropic_dilated_project);

    m.def("m2_convection", ::lietorch::m2::convection);

    m.def(
        "m2_linear_convolution",
        ::lietorch::m2::linear_convolution);

    m.def(
        "m2_morphological_convolution",
        ::lietorch::m2::morphological_convolution);

    m.def(
        "m2_logarithmic_metric_estimate",
        ::lietorch::m2::logarithmic_metric_estimate);
    m.def(
        "m2_logarithmic_metric_estimate_nondiag",
        ::lietorch::m2::logarithmic_metric_estimate_nondiag);

    m.def("m2_morphological_kernel", ::lietorch::m2::morphological_kernel);
    m.def("m2_morphological_kernel_nondiag", 
        ::lietorch::m2::morphological_kernel_nondiag);

    m.def("m2_diffusion_kernel", ::lietorch::m2::diffusion_kernel);
    m.def("m2_diffusion_kernel_nondiag", 
        ::lietorch::m2::diffusion_kernel_nondiag);

    m.def("m2_fractional_dilation", ::lietorch::m2::fractional_dilation);
    m.def("m2_fractional_dilation_nondiag", 
        ::lietorch::m2::fractional_dilation_nondiag);

    m.def("m2_fractional_erosion", ::lietorch::m2::fractional_erosion);
    m.def("m2_fractional_erosion_nondiag", 
        ::lietorch::m2::fractional_erosion_nondiag);

    m.def("m2_linear", ::lietorch::m2::linear);

    m.def(
        "r2_morphological_convolution",
        ::lietorch::r2::morphological_convolution);
    m.def("r2_morphological_kernel", ::lietorch::r2::morphological_kernel);
    m.def("r2_fractional_dilation", ::lietorch::r2::fractional_dilation);
    m.def("r2_fractional_erosion", ::lietorch::r2::fractional_erosion);
    m.def("r2_linear", ::lietorch::r2::linear);
    m.def("r2_convection", ::lietorch::r2::convection);

    m.def("temperate_plus(Tensor a, Tensor b) -> Tensor");
    m.impl("temperate_plus", torch::kCPU, &::lietorch::temperate::plus_cpu);
}
