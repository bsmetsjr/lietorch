#include "generic_cpu.h"
#include "../parallel.h"
#include <algorithm>
#include <array>
#include <iterator>
#include <limits>
#include <numeric>

namespace
{ // internal ops implementations

// portable multi-threaded implementation of grayscale dilation
template <typename scalar_t>
inline void
grayscale_dilation_2d_fw_cpu_mt(
    const torch::Tensor& image,
    const torch::Tensor& filter,
    const torch::Tensor& out,
    const torch::Tensor& backindex)
{
    const auto image_a = image.accessor<scalar_t, 2>();
    const auto filter_a = filter.accessor<scalar_t, 2>();
    auto out_a = out.accessor<scalar_t, 2>();
    auto backindex_a = backindex.accessor<int64_t, 3>();

    const int64_t h = image_a.size(0);
    const int64_t w = image_a.size(1);
    const int64_t fh = filter_a.size(0);
    const int64_t fw = filter_a.size(1);

    const int64_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const int64_t f_bottom = (fh - 1) / 2;
    const int64_t f_left = (fw - 1) / 2 + (fw - 1) % 2;
    const int64_t f_right = (fw - 1) / 2;

    std::vector<int64_t> i_range(h);
    std::iota(i_range.begin(), i_range.end(), 0);

    lietorch::for_each(i_range.begin(), i_range.end(), [&](const int64_t& i) {
        for (int64_t j = 0; j < w; j++)
        {
            scalar_t out_val = std::numeric_limits<scalar_t>::lowest();
            int64_t back_h = 0, back_w = 0;

            for (int64_t k = -std::min<int64_t>(f_top, i);
                 k <= std::min<int64_t>(h - i - 1, f_bottom);
                 k++)
            {
                for (int64_t l = -std::min<int64_t>(f_left, j);
                     l <= std::min<int64_t>(w - j - 1, f_right);
                     l++)
                {
                    const scalar_t new_val = std::max<scalar_t>(
                        out_val,
                        image_a[i + k][j + l] +
                            filter_a[k + f_top][l + f_left]);

                    if (new_val != out_val)
                    {
                        out_val = new_val;
                        back_h = i + k;
                        back_w = j + l;
                    }
                }
            }

            out_a[i][j] = out_val;
            backindex_a[i][j][0] = back_h;
            backindex_a[i][j][1] = back_w;
        }
    });
}

// naive loop-based implementation of grayscale dilation
template <typename scalar_t>
inline void
grayscale_dilation_2d_fw_cpu_naive(
    const torch::Tensor& image,
    const torch::Tensor& filter,
    const torch::Tensor& out,
    const torch::Tensor& backindex)
{
    const auto image_a = image.accessor<scalar_t, 2>();
    const auto filter_a = filter.accessor<scalar_t, 2>();
    auto out_a = out.accessor<scalar_t, 2>();
    auto backindex_a = backindex.accessor<int64_t, 3>();

    const int64_t h = image_a.size(0);
    const int64_t w = image_a.size(1);
    const int64_t fh = filter_a.size(0);
    const int64_t fw = filter_a.size(1);

    const int64_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const int64_t f_bottom = (fh - 1) / 2;
    const int64_t f_left = (fw - 1) / 2 + (fw - 1) % 2;
    const int64_t f_right = (fw - 1) / 2;

    for (int64_t i = 0; i < h; i++)
    {
        for (int64_t j = 0; j < w; j++)
        {
            scalar_t out_val = std::numeric_limits<scalar_t>::lowest();
            int64_t back_h = 0, back_w = 0;

            for (int64_t k = -std::min<int64_t>(f_top, i);
                 k <= std::min<int64_t>(h - i - 1, f_bottom);
                 k++)
            {
                for (int64_t l = -std::min<int64_t>(f_left, j);
                     l <= std::min<int64_t>(w - j - 1, f_right);
                     l++)
                {
                    const scalar_t new_val = std::max<scalar_t>(
                        out_val,
                        image_a[i + k][j + l] +
                            filter_a[k + f_top][l + f_left]);

                    if (new_val != out_val)
                    {
                        out_val = new_val;
                        back_h = i + k;
                        back_w = j + l;
                    }
                }
            }

            out_a[i][j] = out_val;
            backindex_a[i][j][0] = back_h;
            backindex_a[i][j][1] = back_w;
        }
    }
}

// portable multi-threaded implementation of grayscale erosion
template <typename scalar_t>
inline void
grayscale_erosion_2d_fw_cpu_mt(
    const torch::Tensor& image,
    const torch::Tensor& filter,
    const torch::Tensor& out,
    const torch::Tensor& backindex)
{
    const auto image_a = image.accessor<scalar_t, 2>();
    const auto filter_a = filter.accessor<scalar_t, 2>();
    auto out_a = out.accessor<scalar_t, 2>();
    auto backindex_a = backindex.accessor<int64_t, 3>();

    const int64_t h = image_a.size(0);
    const int64_t w = image_a.size(1);
    const int64_t fh = filter_a.size(0);
    const int64_t fw = filter_a.size(1);

    const int64_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const int64_t f_bottom = (fh - 1) / 2;
    const int64_t f_left = (fw - 1) / 2 + (fw - 1) % 2;
    const int64_t f_right = (fw - 1) / 2;

    std::vector<int64_t> i_range(h);
    std::iota(i_range.begin(), i_range.end(), 0);

    lietorch::for_each(i_range.begin(), i_range.end(), [&](const int64_t& i) {
        for (int64_t j = 0; j < w; j++)
        {
            scalar_t out_val = std::numeric_limits<scalar_t>::max();
            int64_t back_h = 0, back_w = 0;

            for (int64_t k = -std::min<int64_t>(f_top, i);
                 k <= std::min<int64_t>(h - i - 1, f_bottom);
                 k++)
            {
                for (int64_t l = -std::min<int64_t>(f_left, j);
                     l <= std::min<int64_t>(w - j - 1, f_right);
                     l++)
                {
                    const scalar_t new_val = std::min<scalar_t>(
                        out_val,
                        image_a[i + k][j + l] -
                            filter_a[k + f_top][l + f_left]);

                    if (new_val != out_val)
                    {
                        out_val = new_val;
                        back_h = i + k;
                        back_w = j + l;
                    }
                }
            }

            out_a[i][j] = out_val;
            backindex_a[i][j][0] = back_h;
            backindex_a[i][j][1] = back_w;
        }
    });
}

// naive loop-based implementation of grayscale erosion
template <typename scalar_t>
inline void
grayscale_erosion_2d_fw_cpu_naive(
    const torch::Tensor& image,
    const torch::Tensor& filter,
    const torch::Tensor& out,
    const torch::Tensor& backindex)
{
    const auto image_a = image.accessor<scalar_t, 2>();
    const auto filter_a = filter.accessor<scalar_t, 2>();
    auto out_a = out.accessor<scalar_t, 2>();
    auto backindex_a = backindex.accessor<int64_t, 3>();

    const int64_t h = image_a.size(0);
    const int64_t w = image_a.size(1);
    const int64_t fh = filter_a.size(0);
    const int64_t fw = filter_a.size(1);

    const int64_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const int64_t f_bottom = (fh - 1) / 2;
    const int64_t f_left = (fw - 1) / 2 + (fw - 1) % 2;
    const int64_t f_right = (fw - 1) / 2;

    for (int64_t i = 0; i < h; i++)
    {
        for (int64_t j = 0; j < w; j++)
        {
            scalar_t out_val = std::numeric_limits<scalar_t>::max();
            int64_t back_h = 0, back_w = 0;

            for (int64_t k = -std::min<int64_t>(f_top, i);
                 k <= std::min<int64_t>(h - i - 1, f_bottom);
                 k++)
            {
                for (int64_t l = -std::min<int64_t>(f_left, j);
                     l <= std::min<int64_t>(w - j - 1, f_right);
                     l++)
                {
                    const scalar_t new_val = std::min<scalar_t>(
                        out_val,
                        image_a[i + k][j + l] -
                            filter_a[k + f_top][l + f_left]);

                    if (new_val != out_val)
                    {
                        out_val = new_val;
                        back_h = i + k;
                        back_w = j + l;
                    }
                }
            }

            out_a[i][j] = out_val;
            backindex_a[i][j][0] = back_h;
            backindex_a[i][j][1] = back_w;
        }
    }
}

// portable multi-threaded implementation of backpropagation of 2D dilation
template <typename scalar_t>
inline void
grayscale_dilation_2d_bw_cpu_mt(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const torch::Tensor& image_grad,
    const torch::Tensor& filter_grad)
{
    const auto backindex_a = backindex.accessor<int64_t, 3>();
    const auto grad_a = grad.accessor<scalar_t, 2>();
    auto image_grad_a = image_grad.accessor<scalar_t, 2>();
    auto filter_grad_a = filter_grad.accessor<scalar_t, 2>();

    const int64_t h = image_grad_a.size(0);
    const int64_t w = image_grad_a.size(1);
    const int64_t fh = filter_grad_a.size(0);
    const int64_t fw = filter_grad_a.size(1);

    const int64_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const int64_t f_left = (fw - 1) / 2 + (fw - 1) % 2;

    std::vector<int64_t> i_range(h);
    std::iota(i_range.begin(), i_range.end(), 0);

    lietorch::for_each(i_range.begin(), i_range.end(), [&](const int64_t& i) {
        for (int64_t j = 0; j < w; j++)
        {
            const int64_t back_h = backindex_a[i][j][0];
            const int64_t back_w = backindex_a[i][j][1];
            image_grad_a[back_h][back_w] += grad_a[i][j];

            const int64_t back_filter_h = back_h - i + f_top;
            const int64_t back_filter_w = back_w - j + f_left;

            filter_grad_a[back_filter_h][back_filter_w] += grad_a[i][j];
        }
    });
}

// portable multi-threaded implementation of backpropagation of 2D dilation
template <typename scalar_t>
inline void
grayscale_erosion_2d_bw_cpu_mt(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const torch::Tensor& image_grad,
    const torch::Tensor& filter_grad)
{
    const auto backindex_a = backindex.accessor<int64_t, 3>();
    const auto grad_a = grad.accessor<scalar_t, 2>();
    auto image_grad_a = image_grad.accessor<scalar_t, 2>();
    auto filter_grad_a = filter_grad.accessor<scalar_t, 2>();

    const int64_t h = image_grad_a.size(0);
    const int64_t w = image_grad_a.size(1);
    const int64_t fh = filter_grad_a.size(0);
    const int64_t fw = filter_grad_a.size(1);

    const int64_t f_top = (fh - 1) / 2 + (fh - 1) % 2;
    const int64_t f_left = (fw - 1) / 2 + (fw - 1) % 2;

    std::vector<int64_t> i_range(h);
    std::iota(i_range.begin(), i_range.end(), 0);

    lietorch::for_each(i_range.begin(), i_range.end(), [&](const int64_t& i) {
        for (int64_t j = 0; j < w; j++)
        {
            const int64_t back_h = backindex_a[i][j][0];
            const int64_t back_w = backindex_a[i][j][1];
            image_grad_a[back_h][back_w] += grad_a[i][j];

            const int64_t back_filter_h = back_h - i + f_top;
            const int64_t back_filter_w = back_w - j + f_left;

            filter_grad_a[back_filter_h][back_filter_w] -= grad_a[i][j];
        }
    });
}

} // namespace

/*



*/

namespace lietorch
{
namespace generic
{

// Addition forward
torch::Tensor
add_fw_cpu(const torch::Tensor& a, const torch::Tensor& b)
{
    return a + b;
}

// Addition backward
std::tuple<torch::Tensor, torch::Tensor>
add_bw_cpu(const torch::Tensor& grad)
{
    return std::make_tuple(grad, grad);
}

// Grayscale dilation 2D, forward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_fw_cpu(
    const torch::Tensor& image, const torch::Tensor& filter)
{
    std::vector<int64_t> backindex_size = image.sizes().vec();
    backindex_size.push_back(2);
    torch::Tensor out = torch::empty_like(image);
    torch::Tensor backindex = torch::empty(
        backindex_size,
        torch::TensorOptions().dtype(torch::kInt64).device(image.device()));

    AT_DISPATCH_ALL_TYPES(image.scalar_type(), __func__, [&] {
        grayscale_dilation_2d_fw_cpu_mt<scalar_t>(
            image, filter, out, backindex);
    });

    return std::make_tuple(out, backindex);
}

// Grayscale dilation 2D, backward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_bw_cpu(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w)
{
    auto image_grad = torch::zeros_like(grad);
    auto filter_grad = torch::zeros(
        {filter_h, filter_w}, torch::TensorOptions().dtype(grad.dtype()));

    AT_DISPATCH_ALL_TYPES(grad.scalar_type(), __func__, [&] {
        grayscale_dilation_2d_bw_cpu_mt<scalar_t>(
            backindex, grad, image_grad, filter_grad);
    });

    return std::make_tuple(image_grad, filter_grad);
}

// Grayscale erosion 2D, forward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_fw_cpu(
    const torch::Tensor& image, const torch::Tensor& filter)
{
    std::vector<int64_t> backindex_size = image.sizes().vec();
    backindex_size.push_back(2);
    torch::Tensor out = torch::empty_like(image);
    torch::Tensor backindex = torch::empty(
        backindex_size,
        torch::TensorOptions().dtype(torch::kInt64).device(image.device()));

    AT_DISPATCH_ALL_TYPES(image.scalar_type(), __func__, [&] {
        grayscale_erosion_2d_fw_cpu_mt<scalar_t>(image, filter, out, backindex);
    });

    return std::make_tuple(out, backindex);
}

// Grayscale erosion 2D, backward
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_bw_cpu(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w)
{
    auto image_grad = torch::zeros_like(grad);
    auto filter_grad = torch::zeros(
        {filter_h, filter_w}, torch::TensorOptions().dtype(grad.dtype()));

    AT_DISPATCH_ALL_TYPES(grad.scalar_type(), __func__, [&] {
        grayscale_erosion_2d_bw_cpu_mt<scalar_t>(
            backindex, grad, image_grad, filter_grad);
    });

    return std::make_tuple(image_grad, filter_grad);
}

} // namespace generic
} // namespace lietorch