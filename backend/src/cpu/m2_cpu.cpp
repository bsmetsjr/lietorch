#include "m2_cpu.h"
#include "../m2_element.h"
#include "../parallel.h"
#include "m2_interpolation_cpu.h"

#include <algorithm>
#define _USE_MATH_DEFINES
#include <cmath>
#include <limits>
#include <numeric>
#include <vector>

//#include "../dbg.h"

#pragma warning(push, 1)
#include <c10/core/ScalarType.h>
#pragma warning(pop)

namespace // internal namespace
{

// These are used to map C++ types to ScalarTypes.
// This is copied from
// https://github.com/pytorch/pytorch/blob/master/c10/core/ScalarType.h (master
// @ c070e8f) on 9/04/2020 as it is needed here but is not available in
// pytorch 1.4.0/1.5.0/1.6.0p
// TODO: remove when it comes included in the pytorch release version.

template <typename> struct CPPTypeToScalarType
{
    constexpr static c10::ScalarType value = c10::ScalarType::Undefined;
};

#define SPECIALIZE_CPPTypeToScalarType(cpp_type, scalar_type)                  \
    template <> struct CPPTypeToScalarType<cpp_type>                           \
    {                                                                          \
        constexpr static c10::ScalarType value = c10::ScalarType::scalar_type; \
    };

AT_FORALL_SCALAR_TYPES(SPECIALIZE_CPPTypeToScalarType)

#undef SPECIALIZE_CPPTypeToScalarType

/*

*/
template <typename scalar_t>
inline torch::Tensor
adp_filter(
    const int64_t orientations,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    const int r =
        static_cast<int>(std::floor(std::fmax(longitudinal, lateral) / 2));

    const torch::ScalarType t = CPPTypeToScalarType<scalar_t>::value;
    const auto kernel = torch::empty(
        {orientations, 2 * r + 1, 2 * r + 1}, torch::TensorOptions().dtype(t));
    auto kernel_a = kernel.accessor<scalar_t, 3>();

    const double p = 2 * alpha / (2 * alpha - 1);

    for (int i = 0; i < orientations; i++)
    {
        const double theta = i * M_PI / orientations;
        const double cos_theta = std::cos(theta);
        const double sin_theta = std::sin(theta);

        for (int j = 0; j < 2 * r + 1; j++)
        {
            const double y = static_cast<double>(j - r);

            for (int k = 0; k < 2 * r + 1; k++)
            {
                const double x = static_cast<double>(k - r);
                const double rx =
                    2 * (x * cos_theta + y * sin_theta) / longitudinal;
                const double ry = 2 * (y * cos_theta - x * sin_theta) / lateral;

                const double norm =
                    std::sqrt(std::pow(rx, 2) + std::pow(ry, 2));
                kernel_a[i][j][k] =
                    static_cast<scalar_t>(-scale * std::pow(norm, p));
            }
        }
    }

    return kernel;
}

/*

*/
template <typename scalar_t>
inline void
adp_slice_fw(
    const torch::TensorAccessor<scalar_t, 5> input_a,  // [B,C,Or,H,W]
    const torch::TensorAccessor<scalar_t, 3> kernel_a, // [Or,fH,fW]
    torch::TensorAccessor<scalar_t, 4> out_a,          // [B,C,H,W]
    torch::TensorAccessor<int64_t, 5> backindex_a,     // [B,C,H,W,3]
    const int64_t batch,
    const int64_t channel)
{
    const int64_t ors = input_a.size(2);
    const int64_t h = input_a.size(3);
    const int64_t w = input_a.size(4);
    const int64_t r = (kernel_a.size(1) - 1) / 2;

    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            scalar_t out_val = std::numeric_limits<scalar_t>::lowest();
            int64_t back_or = 0, back_h = 0, back_w = 0;

            for (int64_t k = -std::min<int64_t>(r, i);
                 k <= std::min<int64_t>(h - i - 1, r);
                 k++)
            {
                for (int64_t l = -std::min<int64_t>(r, j);
                     l <= std::min<int64_t>(w - j - 1, r);
                     l++)
                {
                    for (int64_t m = 0; m < ors; m++)
                    {
                        const scalar_t new_val = std::max<scalar_t>(
                            out_val,
                            input_a[batch][channel][m][i + k][j + l] +
                                kernel_a[m][k + r][l + r]);

                        if (new_val != out_val)
                        {
                            out_val = new_val;
                            back_or = m;
                            back_h = i + k;
                            back_w = j + l;
                        }
                    }
                }
            }

            out_a[batch][channel][i][j] = out_val;
            backindex_a[batch][channel][i][j][0] = back_or;
            backindex_a[batch][channel][i][j][1] = back_h;
            backindex_a[batch][channel][i][j][2] = back_w;
        }
    }
}

/*

*/
template <typename scalar_t>
inline void
anisotropic_dilated_project_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> input_a, // shape [B,C,Or,H,W]
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale,
    torch::TensorAccessor<scalar_t, 4> out_a,
    torch::TensorAccessor<int64_t, 5> backindex_a)
{
    const auto orientations = input_a.size(2);
    const torch::Tensor kernel =
        adp_filter<scalar_t>(orientations, longitudinal, lateral, alpha, scale);
    const auto kernel_a = kernel.accessor<scalar_t, 3>();

    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) {
                    adp_slice_fw(input_a, kernel_a, out_a, backindex_a, b, c);
                });
        });
}

/*

*/
template <typename scalar_t>
inline void
adp_slice_bw(
    const torch::TensorAccessor<int64_t, 5> backindex_a, // [B,C,H,W,3]
    const torch::TensorAccessor<scalar_t, 4> grad_a,     // [B,C,H,W]
    torch::TensorAccessor<scalar_t, 5> input_grad_a,     // [B,C,Or,H,W]
    const int64_t batch,
    const int64_t channel)
{
    const int64_t ors = input_grad_a.size(2);
    const int64_t h = input_grad_a.size(3);
    const int64_t w = input_grad_a.size(4);

    for (int64_t i = 0; i < h; i++)
    {
        for (int64_t j = 0; j < w; j++)
        {
            const int64_t back_or = backindex_a[batch][channel][i][j][0];
            const int64_t back_h = backindex_a[batch][channel][i][j][1];
            const int64_t back_w = backindex_a[batch][channel][i][j][2];

            input_grad_a[batch][channel][back_or][back_h][back_w] +=
                grad_a[batch][channel][i][j];
        }
    }
}

/*

*/
template <typename scalar_t>
inline void
anisotropic_dilated_project_bw_cpu_impl(
    const torch::TensorAccessor<int64_t, 5> backindex_a, // [B,C,H,W,3]
    const torch::TensorAccessor<scalar_t, 4> grad_a,     // [B,C,H,W]
    torch::TensorAccessor<scalar_t, 5> input_grad_a)     // [B,C,Or,H,W]
{
    std::vector<int64_t> batch_range(grad_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(grad_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) {
                    adp_slice_bw(backindex_a, grad_a, input_grad_a, b, c);
                });
        });
}

/*

*/
template <typename scalar_t, typename coordinate_t = float>
void
convection_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> input_a,   // [B,C,Or,H,W]
    const torch::TensorAccessor<coordinate_t, 2> g0_a,  // [C,3]
    torch::TensorAccessor<scalar_t, 5> out_a,           // [B,C,Or,H,W]
    torch::TensorAccessor<scalar_t, 6> out_grad_field_a // [B,C,Or,H,W,3]
) 
{
    const int64_t ors = input_a.size(2); // number of orientations
    const int64_t h = input_a.size(3);   // number of y's
    const int64_t w = input_a.size(4);   // number of x's

    // convert int64_t sizes to coordinate_t for use in calculations
    const coordinate_t ors_co = static_cast<coordinate_t>(ors);
    const coordinate_t h_co = static_cast<coordinate_t>(h);
    const coordinate_t w_co = static_cast<coordinate_t>(w);

    // Lambda for doing convection for a single channel and single
    // batch-element, i.e. 'instance'.
    auto convection_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        const auto input = input_a[b][c];
        auto out = out_a[b][c];
        auto out_grad_field = out_grad_field_a[b][c];

        using element = ::lietorch::m2::element<coordinate_t>;
        const element g0(g0_a[c][0], g0_a[c][1], g0_a[c][2], ors_co);

        for (int64_t i = 0; i < ors; i++)
        {
            for (int64_t j = 0; j < h; j++)
            {
                for (int64_t k = 0; k < w; k++)
                {
                    const element gp(
                        static_cast<coordinate_t>(i),
                        static_cast<coordinate_t>(j),
                        static_cast<coordinate_t>(k),
                        ors_co);
                    const element sample = gp * g0.inv();

                    const auto [val, dz, dy, dx] = ::lietorch::m2::
                        interpolate_trilinear_with_grad<scalar_t, coordinate_t>(
                            input, sample);

                    out[i][j][k] = val;
                    out_grad_field[i][j][k][0] = dz;
                    out_grad_field[i][j][k][1] = dy;
                    out_grad_field[i][j][k][2] = dx;
                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { convection_fw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t, typename coordinate_t = float>
void
convection_bw_cpu_impl(
    const torch::TensorAccessor<coordinate_t, 2> g0_a,         // [C,3]
    const torch::TensorAccessor<scalar_t, 5> grad_a,           // [B,C,Or,H,W]
    torch::TensorAccessor<scalar_t, 5> input_grad_a,           // [B,C,Or,H,W]
    const torch::TensorAccessor<scalar_t, 6> out_grad_field_a, // [B,C,Or,H,W,3]
    torch::TensorAccessor<coordinate_t, 6> g0_splits_a         // [B,C,Or,H,W,3]
)        
{
    const int64_t ors = grad_a.size(2); // number of orientations
    const int64_t h = grad_a.size(3);   // number of y's
    const int64_t w = grad_a.size(4);   // number of x's

    // convert int64_t sizes to coordinate_t for use in calculations
    const coordinate_t ors_co = static_cast<coordinate_t>(ors);
    const coordinate_t h_co = static_cast<coordinate_t>(h);
    const coordinate_t w_co = static_cast<coordinate_t>(w);

    // Backer_ward for one batch and channel ori 'instance'.
    auto transport_bw_one_instance = [&](const int64_t& b, const int64_t c) {
        const auto grad = grad_a[b][c];
        auto in_grad = input_grad_a[b][c];
        const auto out_grad_field = out_grad_field_a[b][c];
        auto g0_splits = g0_splits_a[b][c];

        const auto z0 = g0_a[c][0];
        const auto y0 = g0_a[c][1];
        const auto x0 = g0_a[c][2];

        const auto theta0 = static_cast<coordinate_t>(2 * M_PI * z0 / ors_co);
        const auto a1 = -cos(theta0) * x0 - sin(theta0) * y0;
        const auto a2 = sin(theta0) * x0 - cos(theta0) * y0;

        for (int64_t i = 0; i < ors; i++)
        {
            const coordinate_t theta =
                static_cast<coordinate_t>(2 * M_PI * i / ors_co);
            const coordinate_t cos_theta = cos(theta);
            const coordinate_t sin_theta = sin(theta);

            const coordinate_t j00 = -1;
            const coordinate_t j01 =
                static_cast<coordinate_t>(2 * M_PI / ors_co) *
                (sin_theta * a2 - cos_theta * a1);
            const coordinate_t j02 =
                static_cast<coordinate_t>(2 * M_PI / ors_co) *
                (cos_theta * a2 + sin_theta * a1);

            const coordinate_t j10 = 0;
            const coordinate_t j11 =
                -sin_theta * sin(theta0) - cos_theta * cos(theta0);
            const coordinate_t j12 =
                -cos_theta * sin(theta0) + sin_theta * cos(theta0);

            const coordinate_t j20 = 0;
            const coordinate_t j21 =
                -sin_theta * cos(theta0) + cos_theta * sin(theta0);
            const coordinate_t j22 =
                -cos_theta * cos(theta0) - sin_theta * sin(theta0);

            const coordinate_t sample_i =
                fmod(static_cast<coordinate_t>(i - z0), ors_co);
            const coordinate_t sample_i_floor = floor(sample_i);
            const coordinate_t offset_i = sample_i - sample_i_floor;

            const int64_t base_i0 = static_cast<int64_t>(
                sample_i_floor >= 0 ? sample_i_floor : sample_i_floor + ors_co);
            const int64_t base_i1 = (base_i0 + 1) % ors;

            for (int64_t j = 0; j < h; j++)
            {
                const coordinate_t sample_j = static_cast<coordinate_t>(j) +
                                              sin_theta * a1 + cos_theta * a2;

                if (sample_j <= -1 || sample_j >= h)
                {
                    continue;
                }

                const int64_t base_j = static_cast<int64_t>(floor(sample_j));
                const coordinate_t offset_j = sample_j - floor(sample_j);

                for (int64_t k = 0; k < w; k++)
                {
                    const coordinate_t sample_k = static_cast<coordinate_t>(k) +
                                                  cos_theta * a1 -
                                                  sin_theta * a2;

                    if (sample_k <= -1 || sample_k >= w)
                    {
                        continue;
                    }

                    const int64_t base_k =
                        static_cast<int64_t>(floor(sample_k));
                    const coordinate_t offset_k = sample_k - floor(sample_k);

                    const scalar_t w000 = static_cast<scalar_t>(
                        (1 - offset_i) * (1 - offset_j) * (1 - offset_k));
                    const scalar_t w100 = static_cast<scalar_t>(
                        offset_i * (1 - offset_j) * (1 - offset_k));
                    const scalar_t w010 = static_cast<scalar_t>(
                        (1 - offset_i) * offset_j * (1 - offset_k));
                    const scalar_t w110 = static_cast<scalar_t>(
                        offset_i * offset_j * (1 - offset_k));
                    const scalar_t w001 = static_cast<scalar_t>(
                        (1 - offset_i) * (1 - offset_j) * offset_k);
                    const scalar_t w101 = static_cast<scalar_t>(
                        offset_i * (1 - offset_j) * offset_k);
                    const scalar_t w011 = static_cast<scalar_t>(
                        (1 - offset_i) * offset_j * offset_k);
                    const scalar_t w111 =
                        static_cast<scalar_t>(offset_i * offset_j * offset_k);

                    const auto gval = grad[i][j][k];
                    if (base_k >= 0)
                    {
                        if (base_j >= 0)
                        {
                            in_grad[base_i0][base_j][base_k] += w000 * gval;
                            in_grad[base_i1][base_j][base_k] += w100 * gval;
                        }
                        if (base_j + 1 < h)
                        {
                            in_grad[base_i0][base_j + 1][base_k] += w010 * gval;
                            in_grad[base_i1][base_j + 1][base_k] += w110 * gval;
                        }
                    }
                    if (base_k + 1 < w)
                    {
                        if (base_j >= 0)
                        {
                            in_grad[base_i0][base_j][base_k + 1] += w001 * gval;
                            in_grad[base_i1][base_j][base_k + 1] += w101 * gval;
                        }
                        if (base_j + 1 < h)
                        {
                            in_grad[base_i0][base_j + 1][base_k + 1] +=
                                w011 * gval;
                            in_grad[base_i1][base_j + 1][base_k + 1] +=
                                w111 * gval;
                        }
                    }
                    const coordinate_t dz =
                        static_cast<coordinate_t>(out_grad_field[i][j][k][0]);
                    const coordinate_t dy =
                        static_cast<coordinate_t>(out_grad_field[i][j][k][1]);
                    const coordinate_t dx =
                        static_cast<coordinate_t>(out_grad_field[i][j][k][2]);

                    const coordinate_t gval_co =
                        static_cast<coordinate_t>(gval);
                    g0_splits[i][j][k][0] =
                        gval_co * (j00 * dz + j01 * dy + j02 * dx);
                    g0_splits[i][j][k][1] =
                        gval_co * (j10 * dz + j11 * dy + j12 * dx);
                    g0_splits[i][j][k][2] =
                        gval_co * (j20 * dz + j21 * dy + j22 * dx);
                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(grad_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(grad_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { transport_bw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t>
void
linear_convolution_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> input_a,  // [B,C,Ors,W,H]
    const torch::TensorAccessor<scalar_t, 5> kstack_a, // [Ors,C,sOrs,sW,sH]
    torch::TensorAccessor<scalar_t, 5> out_a           // [B,C,Ors,W,H]
)
{
    const int64_t ors = input_a.size(2); // number of orientations
    const int64_t h = input_a.size(3);   // number of y's
    const int64_t w = input_a.size(4);   // number of x's

    // These should all be odd ...
    const int64_t stack_ors = kstack_a.size(2);
    const int64_t stack_h = kstack_a.size(3);
    const int64_t stack_w = kstack_a.size(4);

    // ... so that this makes sense
    const int64_t r_ors = (stack_ors - 1) / 2;
    const int64_t r_h = (stack_h - 1) / 2;
    const int64_t r_w = (stack_w - 1) / 2;

    auto mconv_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        for (int64_t i = 0; i < ors; i++)
        {
            for (int64_t j = 0; j < h; j++)
            {
                const auto j_stack_from = std::max<int64_t>(0, r_h - j);
                const auto j_stack_to = std::min<int64_t>(stack_h, h + r_h - j);

                for (int64_t k = 0; k < w; k++)
                {
                    const auto k_stack_from = std::max<int64_t>(0, r_w - k);
                    const auto k_stack_to = std::min<int64_t>(stack_w, w + r_w - k);

                    scalar_t val = std::numeric_limits<scalar_t>::max();
                    int64_t hit_i, hit_j, hit_k;

                    for (int64_t i_stack = 0; i_stack < stack_ors; i_stack++)
                    {
                        for (int64_t j_stack = j_stack_from; j_stack < j_stack_to; j_stack++)
                        {
                            for (int64_t k_stack = k_stack_from; k_stack < k_stack_to; k_stack++)
                            {
                                // positive modulus
                                const int64_t i_mod = ((i - r_ors + i_stack) % ors + ors) % ors;
                                
                                const auto in = input_a[b][c][i_mod][j - r_h + j_stack][k - r_w + k_stack];
                                const auto ker = kstack_a[i][c][i_stack][j_stack][k_stack];

                                out_a[b][c][i][j][k] += in *  ker;
                            }
                        }
                    }
                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_fw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t>
void
linear_convolution_bw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> grad_a,   //[B,C,Ors,W,H]
    const torch::TensorAccessor<scalar_t, 5> input_a,  //[B,C,Ors,H,W]
    const torch::TensorAccessor<scalar_t, 4> kernel_a, //[C,kOrs,kW,kH]
    torch::TensorAccessor<scalar_t, 5> input_grad_a,   //[B,C,Ors,W,H]
    torch::TensorAccessor<scalar_t, 5> kernel_grad_a   //[B,C,kOrs,kW,kH]
)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int64_t ors = grad_a.size(2); // number of orientations
    const int64_t h = grad_a.size(3);   // number of y's
    const int64_t w = grad_a.size(4);   // number of x's

    const int64_t ker_ors = kernel_grad_a.size(2);
    const int64_t ker_h = kernel_grad_a.size(3);
    const int64_t ker_w = kernel_grad_a.size(4);

    const int64_t stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const int64_t stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const int64_t stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const int64_t r_ors = (stack_ors - 1) / 2;
    const int64_t r_h = (stack_h - 1) / 2;
    const int64_t r_w = (stack_w - 1) / 2;

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / 2.0f;
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / 2.0f;
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / 2.0f;
    const element g0(0.0, offset_y, offset_x);

    auto mconv_bw_one_instance = [&](const int64_t& b, const int64_t& c) {

        // loop over elements of grad_a[b][c][i][j][k]
        for (int64_t i = 0; i < ors; i++)
        {
            const co_t theta = static_cast<co_t>(2.0 * M_PI) *
                               static_cast<co_t>(i) / static_cast<co_t>(ors);
            const element gtheta(theta, 0.0f, 0.0f); // rotate
            // shift center to origin, rotate and shift back.
            const element grot = g0 * gtheta.inv() * g0.inv();

            for (int64_t j = 0; j < h; j++)
            {
                const auto j_stack_from = std::max<int64_t>(0, r_h - j);
                const auto j_stack_to = std::min<int64_t>(stack_h, h + r_h - j);

                for (int64_t k = 0; k < w; k++)
                {
                    const auto k_stack_from = std::max<int64_t>(0, r_w - k);
                    const auto k_stack_to = std::min<int64_t>(stack_w, w + r_w - k);

                    const auto gradval = grad_a[b][c][i][j][k];

                    // loop over the kernel stack
                    for (int64_t i_stack = 0; i_stack < stack_ors; i_stack++)
                    {
                        for (int64_t j_stack = j_stack_from; j_stack < j_stack_to; j_stack++)
                        {
                            for (int64_t k_stack = k_stack_from; k_stack < k_stack_to; k_stack++)
                            {
                                const int64_t i_in = ((i - r_ors + i_stack) % ors + ors) % ors;
                                const int64_t j_in = j - r_h + j_stack;
                                const int64_t k_in = k - r_w + k_stack;

                                const co_t i_sample = static_cast<co_t>(i_stack * ker_ors) /
                                                    static_cast<co_t>(stack_ors);
                                const co_t j_sample = static_cast<co_t>(j_stack * ker_h) /
                                                    static_cast<co_t>(stack_h);
                                const co_t k_sample = static_cast<co_t>(k_stack * ker_w) /
                                                    static_cast<co_t>(stack_w);
                                const element gker(
                                    i_sample, j_sample, k_sample, static_cast<co_t>(ker_ors));
                                const element gsample = grot * gker;

                                const int64_t ker_i = std::lround(i_sample);
                                const int64_t ker_j = std::lround(gsample.y);
                                const int64_t ker_k = std::lround(gsample.x);

                                if (ker_i >= 0 && ker_i < ker_ors && 
                                    ker_j >= 0 && ker_j < ker_h && 
                                    ker_k >= 0 && ker_k < ker_w
                                )
                                {
                                    const auto in = input_a[b][c][i_in][j_in][k_in];
                                    const auto ker = kernel_a[c][ker_i][ker_j][ker_k];

                                    input_grad_a[b][c][i_in][j_in][k_in] += gradval * ker;
                                    kernel_grad_a[b][c][ker_i][ker_j][ker_k] += gradval * in;
                                }

                                
                            }
                        }
                    }

                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(grad_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(grad_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_bw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t>
void
morphological_convolution_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> input_a,  //[B,C,Ors,W,H]
    const torch::TensorAccessor<scalar_t, 5> kstack_a, //[Ors,C,sOrs,sW,sH]
    torch::TensorAccessor<scalar_t, 5> out_a,          //[B,C,Ors,W,H]
    torch::TensorAccessor<int64_t, 6> backindex_a      //[B,C,Or,H,W,3]
)
{
    const int64_t ors = input_a.size(2); // number of orientations
    const int64_t h = input_a.size(3);   // number of y's
    const int64_t w = input_a.size(4);   // number of x's

    // These should all be odd ...
    const int64_t stack_ors = kstack_a.size(2);
    const int64_t stack_h = kstack_a.size(3);
    const int64_t stack_w = kstack_a.size(4);

    // ... so that this makes sense
    const int64_t r_ors = (stack_ors - 1) / 2;
    const int64_t r_h = (stack_h - 1) / 2;
    const int64_t r_w = (stack_w - 1) / 2;

    auto mconv_fw_one_instance = [&](const int64_t& b, const int64_t& c) {
        for (int64_t i = 0; i < ors; i++)
        {
            for (int64_t j = 0; j < h; j++)
            {
                const auto j_stack_from = std::max<int64_t>(0, r_h - j);
                const auto j_stack_to = std::min<int64_t>(stack_h, h + r_h - j);

                for (int64_t k = 0; k < w; k++)
                {
                    const auto k_stack_from = std::max<int64_t>(0, r_w - k);
                    const auto k_stack_to = std::min<int64_t>(stack_w, w + r_w - k);

                    scalar_t val = std::numeric_limits<scalar_t>::max();
                    int64_t hit_i, hit_j, hit_k;

                    for (int64_t i_stack = 0; i_stack < stack_ors; i_stack++)
                    {
                        for (int64_t j_stack = j_stack_from; j_stack < j_stack_to; j_stack++)
                        {
                            for (int64_t k_stack = k_stack_from; k_stack < k_stack_to; k_stack++)
                            {
                                // positive modulus
                                const int64_t i_in = ((i - r_ors + i_stack) % ors + ors) % ors;
                                const int64_t j_in = j - r_h + j_stack;
                                const int64_t k_in = k - r_w + k_stack;
                                
                                const auto in = input_a[b][c][i_in][j_in][k_in];
                                const auto ker = kstack_a[i][c][i_stack][j_stack][k_stack];

                                const auto newval = in + ker;
                                if (newval < val)
                                {
                                    val = newval;

                                    hit_i = i - r_ors + i_stack;
                                    hit_j = j_in;
                                    hit_k = k_in;
                                }
                            }
                        }
                    }

                    out_a[b][c][i][j][k] = val;
                    backindex_a[b][c][i][j][k][0] = hit_i;
                    backindex_a[b][c][i][j][k][1] = hit_j;
                    backindex_a[b][c][i][j][k][2] = hit_k;
                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(input_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(input_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_fw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t>
void
morphological_convolution_bw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> grad_a,     //[B,C,Ors,W,H]
    const torch::TensorAccessor<int64_t, 6> backindex_a, //[B,C,Or,H,W,3]
    torch::TensorAccessor<scalar_t, 5> input_grad_a,     //[B,C,Ors,W,H]
    torch::TensorAccessor<scalar_t, 5> kernel_grad_a     //[B,C,kOrs,kW,kH]
)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int64_t ors = grad_a.size(2); // number of orientations
    const int64_t h = grad_a.size(3);   // number of y's
    const int64_t w = grad_a.size(4);   // number of x's

    const int64_t ker_ors = kernel_grad_a.size(2);
    const int64_t ker_h = kernel_grad_a.size(3);
    const int64_t ker_w = kernel_grad_a.size(4);

    const int64_t stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const int64_t stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const int64_t stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const int64_t r_ors = (stack_ors - 1) / 2;
    const int64_t r_h = (stack_h - 1) / 2;
    const int64_t r_w = (stack_w - 1) / 2;

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / 2.0f;
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / 2.0f;
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / 2.0f;
    const element g0(0.0, offset_y, offset_x);

    auto mconv_bw_one_instance = [&](const int64_t& b, const int64_t& c) {
        for (int64_t i = 0; i < ors; i++)
        {
            const co_t theta = static_cast<co_t>(2.0 * M_PI) *
                               static_cast<co_t>(i) / static_cast<co_t>(ors);
            const element gtheta(theta, 0.0f, 0.0f); // rotate
            // shift center to origin, rotate and shift back.
            const element grot = g0 * gtheta.inv() * g0.inv();

            for (int64_t j = 0; j < h; j++)
            {
                for (int64_t k = 0; k < w; k++)
                {
                    const auto hit_i = backindex_a[b][c][i][j][k][0];
                    const auto hit_j = backindex_a[b][c][i][j][k][1];
                    const auto hit_k = backindex_a[b][c][i][j][k][2];
                    const auto gradval = grad_a[b][c][i][j][k];

                    const auto hit_i_mod = (hit_i % ors + ors) % ors;

                    input_grad_a[b][c][hit_i_mod][hit_j][hit_k] += gradval;

                    const auto stack_i = hit_i - i + r_ors;
                    const auto stack_j = hit_j - j + r_h;
                    const auto stack_k = hit_k - k + r_w;

                    const co_t i_sample = static_cast<co_t>(stack_i * ker_ors) /
                                          static_cast<co_t>(stack_ors);
                    const co_t j_sample = static_cast<co_t>(stack_j * ker_h) /
                                          static_cast<co_t>(stack_h);
                    const co_t k_sample = static_cast<co_t>(stack_k * ker_w) /
                                          static_cast<co_t>(stack_w);
                    const element gker(
                        i_sample, j_sample, k_sample, static_cast<co_t>(ker_ors));
                    const element gsample = grot * gker;

                    const int64_t ker_i = std::lround(i_sample);
                    const int64_t ker_j = std::lround(gsample.y);
                    const int64_t ker_k = std::lround(gsample.x);

                    if (ker_i >= 0 && ker_i < ker_ors && 
                        ker_j >= 0 && ker_j < ker_h && 
                        ker_k >= 0 && ker_k < ker_w
                    )
                    {

                        kernel_grad_a[b][c][ker_i][ker_j][ker_k] += gradval;
                    }
                }
            }
        }
    };

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(grad_a.size(0));
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(grad_a.size(1));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c) { mconv_bw_one_instance(b, c); });
        });
}

/*

*/
template <typename scalar_t>
void
rotated_kernel_stack_nearest(
    const torch::TensorAccessor<scalar_t, 4> kernel_a, // [C,kOr,kH,kW]
    torch::TensorAccessor<scalar_t, 5> kstack_a,       // [Or,C,sOr,sH,sW]
    const scalar_t boundary
)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const auto ors = kstack_a.size(0);

    const auto ker_ors = kernel_a.size(1);
    const auto ker_h = kernel_a.size(2);
    const auto ker_w = kernel_a.size(3);

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / 2.0f;
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / 2.0f;
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / 2.0f;

    // stack_ors, stack_h, and stack_w should be all odd.
    const auto stack_ors = kstack_a.size(2);
    const auto stack_h = kstack_a.size(3);
    const auto stack_w = kstack_a.size(4);

    const element g0(0.0f, offset_y, offset_x);

    std::vector<int64_t> channel_range(kernel_a.size(0));
    std::iota(channel_range.begin(), channel_range.end(), 0);

    lietorch::for_each(
        channel_range.begin(), channel_range.end(), [&](const int64_t& c) {
            for (int ori = 0; ori < ors; ori ++)
            {
                const co_t theta = static_cast<co_t>(2.0 * M_PI) *
                                   static_cast<co_t>(ori) /
                                   static_cast<co_t>(ors);
                const element gtheta(theta, 0.0f, 0.0f); // rotate
                // shift center to origin, rotate and shift back.
                const element grot = g0 * gtheta.inv() * g0.inv();

                for (int i = 0; i < stack_ors; i++)
                {
                    for (int j = 0; j < stack_h; j++)
                    {
                        for (int k = 0; k < stack_w; k++)
                        {
                            const co_t i_sample = static_cast<co_t>(i * ker_ors) /
                                                  static_cast<co_t>(stack_ors);
                            const co_t j_sample = static_cast<co_t>(j * ker_h) /
                                                  static_cast<co_t>(stack_h);
                            const co_t k_sample = static_cast<co_t>(k * ker_w) /
                                                  static_cast<co_t>(stack_w);
                            const element gker(
                                i_sample,
                                j_sample,
                                k_sample,
                                static_cast<co_t>(ker_ors));
                            const element gsample = grot * gker;

                            kstack_a[ori][c][i][j][k] = ::lietorch::m2::
                                interpolate_nearest_non_periodic<
                                    scalar_t,
                                    co_t>(
                                    kernel_a[c],
                                    i_sample,
                                    gsample.y,
                                    gsample.x,
                                    boundary);
                        }
                    }
                }
            }
        });
}

/*

*/
template <typename scalar_t>
void
linear_fw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> input_a,
    const torch::TensorAccessor<scalar_t, 2> weight_a,
    torch::TensorAccessor<scalar_t, 5> out_a
)
{
    const auto B = input_a.size(0);
    const auto Cin = input_a.size(1);
    const auto Or = input_a.size(2);
    const auto H = input_a.size(3);
    const auto W = input_a.size(4);
    const auto Cout = weight_a.size(1);

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(B);
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(Cout);
    std::iota(channel_range.begin(), channel_range.end(), 0);

    auto linear_fw_one_instance = [&](const int64_t& b, const int64_t& c_out) {
        for (int64_t i = 0; i < Or; i++)
        {
            for (int64_t j = 0; j < H; j++)
            {
                for (int64_t k = 0; k < W; k++)
                {
                    for (int64_t c_in = 0; c_in < Cin; c_in++)
                    {
                        out_a[b][c_out][i][j][k] +=
                            weight_a[c_in][c_out] * input_a[b][c_in][i][j][k];
                    }
                }
            }
        }
    };

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c_out) {
                    linear_fw_one_instance(b, c_out);
                });
        });
}

/*

*/
template <typename scalar_t>
void
linear_bw_cpu_impl(
    const torch::TensorAccessor<scalar_t, 5> grad_a,
    const torch::TensorAccessor<scalar_t, 5> input_a,
    const torch::TensorAccessor<scalar_t, 2> weight_a,
    torch::TensorAccessor<scalar_t, 5> input_grad_a,
    torch::TensorAccessor<scalar_t, 3> weight_grad_a
)
{
    const auto B = input_a.size(0);
    const auto Cin = input_a.size(1);
    const auto Or = input_a.size(2);
    const auto H = input_a.size(3);
    const auto W = input_a.size(4);
    const auto Cout = weight_a.size(1);

    // Parallelize over batches and channels.
    std::vector<int64_t> batch_range(B);
    std::iota(batch_range.begin(), batch_range.end(), 0);

    std::vector<int64_t> channel_range(Cin);
    std::iota(channel_range.begin(), channel_range.end(), 0);

    auto linear_bw_one_instance = [&](const int64_t& b, const int64_t& c_in) {
        for (int64_t i = 0; i < Or; i++)
        {
            for (int64_t j = 0; j < H; j++)
            {
                for (int64_t k = 0; k < W; k++)
                {
                    for (int64_t c_out = 0; c_out < Cout; c_out++)
                    {
                        input_grad_a[b][c_in][i][j][k] +=
                            grad_a[b][c_out][i][j][k] * weight_a[c_in][c_out];

                        weight_grad_a[b][c_in][c_out] +=
                            input_a[b][c_in][i][j][k] *
                            grad_a[b][c_out][i][j][k];
                    }
                }
            }
        }
    };

    lietorch::for_each(
        batch_range.begin(), batch_range.end(), [&](const int64_t& b) {
            lietorch::for_each(
                channel_range.begin(),
                channel_range.end(),
                [&](const int64_t& c_in) { linear_bw_one_instance(b, c_in); });
        });
}

} // namespace

namespace lietorch
{
namespace m2
{

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
anisotropic_dilated_project_fw_cpu(
    const torch::Tensor& input, // shape [B,C,Or,H,W]
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    const auto input_size = input.sizes(); // {B,C,Or,H,W}
    const std::vector<int64_t> out_size{
        input_size[0],
        input_size[1],
        input_size[3],
        input_size[4]}; // {B,C,H,W}
    const std::vector<int64_t> backindex_size{
        input_size[0],
        input_size[1],
        input_size[3],
        input_size[4],
        3}; // {B,C,H,W,3}

    const auto out = torch::empty(out_size, input.options());
    const auto backindex = torch::empty(
        backindex_size, torch::TensorOptions().dtype(torch::kInt64));

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        auto input_a = input.accessor<scalar_t, 5>();
        auto out_a = out.accessor<scalar_t, 4>();
        auto backindex_a = backindex.accessor<int64_t, 5>();

        anisotropic_dilated_project_fw_cpu_impl<scalar_t>(
            input_a, longitudinal, lateral, alpha, scale, out_a, backindex_a);
    });

    return std::make_tuple(out, backindex);
}

/*

*/
torch::Tensor
anisotropic_dilated_project_bw_cpu(
    const torch::Tensor& backindex, // [B,C,H,W,3]
    const torch::Tensor& grad,      // [B,C,H,W]
    const torch::List<int64_t> input_shape)
{
    const auto input_grad = torch::zeros(
        {input_shape[0],
         input_shape[1],
         input_shape[2],
         input_shape[3],
         input_shape[4]},
        torch::TensorOptions().dtype(grad.dtype()));

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        const auto backindex_a = backindex.accessor<int64_t, 5>();
        const auto grad_a = grad.accessor<scalar_t, 4>();
        auto input_grad_a = input_grad.accessor<scalar_t, 5>();

        anisotropic_dilated_project_bw_cpu_impl<scalar_t>(
            backindex_a, grad_a, input_grad_a);
    });

    return input_grad;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cpu(
    const torch::Tensor& input, // [B,C,Or,H,W]
    const torch::Tensor& g0     // [C,3]
)    
{
    const auto out = torch::zeros_like(input);

    std::vector<int64_t> field_size = input.sizes().vec(); // = {B,C,Or,H,W}
    field_size.push_back(3);                               // = {B,C,Or,H,W,3}
    const auto out_grad_field = input.new_zeros(field_size);

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        const auto input_a = input.accessor<scalar_t, 5>();
        const auto out_a = out.accessor<scalar_t, 5>();
        const auto out_grad_field_a = out_grad_field.accessor<scalar_t, 6>();

        switch (g0.scalar_type())
        {

        case at::ScalarType::Float:
        {
            const auto g0_a = g0.accessor<float, 2>();
            convection_fw_cpu_impl<scalar_t, float>(
                input_a, g0_a, out_a, out_grad_field_a);
            break;
        }

        case at::ScalarType::Double:
        {
            const auto g0_a = g0.accessor<double, 2>();
            convection_fw_cpu_impl<scalar_t, double>(
                input_a, g0_a, out_a, out_grad_field_a);
            break;
        }

        default:
            TORCH_CHECK(
                false,
                __func__,
                " not implemented for '",
                toString(g0.scalar_type()),
                "' for the convection vector.");
        }
    });

    return std::make_tuple(out, out_grad_field);
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cpu(
    const torch::Tensor& g0,            // [C,3]
    const torch::Tensor& grad,          // [B,C,Or,H,W]
    const torch::Tensor& out_grad_field // [B,C,Or,H,W,3]
)
{
    using namespace torch::indexing;

    const auto input_grad = torch::zeros_like(grad);
    const auto g0_splits = torch::zeros_like(
        out_grad_field, torch::TensorOptions().dtype(g0.scalar_type()));

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        const auto grad_a = grad.accessor<scalar_t, 5>();
        const auto input_grad_a = input_grad.accessor<scalar_t, 5>();
        const auto out_grad_field_a = out_grad_field.accessor<scalar_t, 6>();

        switch (g0.scalar_type())
        {

        case at::ScalarType::Float:
        {
            const auto g0_a = g0.accessor<float, 2>();
            const auto g0_splits_a = g0_splits.accessor<float, 6>();
            convection_bw_cpu_impl<scalar_t, float>(
                g0_a, grad_a, input_grad_a, out_grad_field_a, g0_splits_a);
            break;
        }

        case at::ScalarType::Double:
        {
            const auto g0_a = g0.accessor<double, 2>();
            const auto g0_splits_a = g0_splits.accessor<double, 6>();
            convection_bw_cpu_impl<scalar_t, double>(
                g0_a, grad_a, input_grad_a, out_grad_field_a, g0_splits_a);
            break;
        }

        default:
            TORCH_CHECK(
                false,
                __func__,
                " not implemented for '",
                toString(g0.scalar_type()),
                "' for the convection vector.");
        }
    });

    const auto g0_grad = g0_splits.sum({0, 2, 3, 4}); // [C,3]

    return std::make_tuple(input_grad, g0_grad);
}

/*

*/
torch::Tensor
linear_convolution_fw_cpu(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    const auto out = torch::zeros_like(input); // [B,C,Ors,H,W]

    const auto c = input.size(1);
    const auto ors = input.size(2);
    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    // We want the kernels of the kernel stack to have odd dimensions so that their centers are integer/well-defined.
    // For example a kernel of size 2 x 2 x 2 has its center at index [0.5, 0.5, 0.5] (where we use zero-indexing).
    // a kernel of size 3 x 3 x 3 has its center at index [1, 1, 1] (where we again use zero-indexing).
    const auto stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const auto stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const auto stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const auto kstack =
        torch::zeros({ors, c, stack_ors, stack_h, stack_w}, kernel.options());

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        const auto input_a = input.accessor<scalar_t, 5>();
        const auto kernel_a = kernel.accessor<scalar_t, 4>();
        auto out_a = out.accessor<scalar_t, 5>();
        auto kstack_a = kstack.accessor<scalar_t, 5>();

        rotated_kernel_stack_nearest<scalar_t>(
            kernel_a, kstack_a, static_cast<scalar_t>(0));

        linear_convolution_fw_cpu_impl<scalar_t>(
            input_a, kstack_a, out_a);
    });

    return out;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_convolution_bw_cpu(
    const torch::Tensor& grad,  // [B,C,Ors,H,W]
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    const auto b = input.size(0);
    const auto c = input.size(1);
    const auto ors = input.size(2);
    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    const auto input_grad = torch::zeros_like(input); // [B,C,Ors,H,W]
    const auto kernel_grad = torch::zeros(
        {b,c,ker_ors,ker_h,ker_w},
        kernel.options()); // [B,C,kOrs,kH,kW]

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        const auto grad_a = grad.accessor<scalar_t, 5>();
        const auto input_a = input.accessor<scalar_t, 5>();
        const auto kernel_a = kernel.accessor<scalar_t, 4>();
        auto input_grad_a = input_grad.accessor<scalar_t, 5>();
        auto kernel_grad_a = kernel_grad.accessor<scalar_t, 5>();

        linear_convolution_bw_cpu_impl<scalar_t>(
            grad_a, input_a, kernel_a, input_grad_a, kernel_grad_a);
    });

    return std::make_tuple(input_grad, kernel_grad.sum({0}));
} // namespace m2

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cpu(
    const torch::Tensor& input, // [B,C,Or,H,W]
    const torch::Tensor& kernel // [C,kOr,kH,kW]
)
{
    const auto out = torch::zeros_like(input); // [B,C,Or,H,W]

    const auto c = input.size(1);
    const auto ors = input.size(2);
    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    // We want the kernels of the kernel stack to have odd dimensions so that their centers are integer/well-defined.
    // For example a kernel of size 2 x 2 x 2 has its center at index [0.5, 0.5, 0.5] (where we use zero-indexing).
    // a kernel of size 3 x 3 x 3 has its center at index [1, 1, 1] (where we again use zero-indexing).
    const auto stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const auto stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const auto stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const auto kstack =
        torch::zeros({ors, c, stack_ors, stack_h, stack_w}, kernel.options()); // [Or,C,kOr2,kH2,kW2]

    std::vector<int64_t> backindex_size = input.sizes().vec(); // {B,C,Or,H,W}
    backindex_size.push_back(3); // {B,C,Or,H,W,3}
    const auto backindex = torch::zeros(
        backindex_size,
        torch::TensorOptions().dtype(torch::kInt64).device(input.device()));

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        const auto input_a = input.accessor<scalar_t, 5>();
        const auto kernel_a = kernel.accessor<scalar_t, 4>();
        auto out_a = out.accessor<scalar_t, 5>();
        auto backindex_a = backindex.accessor<int64_t, 6>();
        auto kstack_a = kstack.accessor<scalar_t, 5>();

        rotated_kernel_stack_nearest<scalar_t>(
            kernel_a, 
            kstack_a, 
            std::numeric_limits<scalar_t>::max()
        );

        morphological_convolution_fw_cpu_impl<scalar_t>(
            input_a, kstack_a, out_a, backindex_a);
    });

    return std::make_tuple(out, backindex);
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cpu(
    const torch::Tensor& grad,            // [B,C,Ors,W,H]
    const torch::Tensor& backindex,       // [B,C,Or,H,W,3]
    const torch::IntArrayRef kernel_sizes // {C,kOrs,kW,kH}
)
{
    const auto input_grad = torch::zeros_like(grad);
    const auto kernel_grad = torch::zeros(
        {grad.size(0),
         kernel_sizes[0],
         kernel_sizes[1],
         kernel_sizes[2],
         kernel_sizes[3]},
        grad.options()); // [B,C,kOrs,kW,kH]
    const auto backindex2 = backindex.to(torch::kInt64);

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        const auto grad_a = grad.accessor<scalar_t, 5>();
        const auto backindex_a = backindex2.accessor<int64_t, 6>();
        auto input_grad_a = input_grad.accessor<scalar_t, 5>();
        auto kernel_grad_a = kernel_grad.accessor<scalar_t, 5>();

        morphological_convolution_bw_cpu_impl<scalar_t>(
            grad_a, backindex_a, input_grad_a, kernel_grad_a);
    });

    return std::make_tuple(input_grad, kernel_grad.sum({0}));
} // namespace m2

/*

*/
torch::Tensor
rotated_kernel_stack_nearest_cpu(
    const torch::Tensor& kernel, // [C,kOr,kH,kW]
    const int64_t orientations   // Or
)
{
    const auto c = kernel.size(0);
    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    const auto stack_ors = (ker_ors % 2 == 0) ? ker_ors + 1 : ker_ors;
    const auto stack_h = (ker_h % 2 == 0) ? ker_h + 1 : ker_h;
    const auto stack_w = (ker_w % 2 == 0) ? ker_w + 1 : ker_w;

    const auto kstack = torch::zeros({orientations, c, stack_ors, stack_h, stack_w}, kernel.options());

    AT_DISPATCH_FLOATING_TYPES(kernel.scalar_type(), __func__, [&] {
        auto kernel_a = kernel.accessor<scalar_t, 4>();
        auto kstack_a = kstack.accessor<scalar_t, 5>();

        scalar_t scalar_max = std::numeric_limits<scalar_t>::max();

        rotated_kernel_stack_nearest<scalar_t>(kernel_a, kstack_a, scalar_max);
    });

    return kstack;
}

/*

*/
torch::Tensor
linear_fw_cpu(
    const torch::Tensor& input, 
    const torch::Tensor& weight
)
{
    const auto B = input.size(0);
    const auto Cin = input.size(1);
    const auto Or = input.size(2);
    const auto H = input.size(3);
    const auto W = input.size(4);
    const auto Cout = weight.size(1);

    const auto out = torch::zeros({B, Cout, Or, H, W}, input.options());

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        auto input_a = input.accessor<scalar_t, 5>();
        auto weight_a = weight.accessor<scalar_t, 2>();
        auto out_a = out.accessor<scalar_t, 5>();

        linear_fw_cpu_impl<scalar_t>(input_a, weight_a, out_a);
    });

    return out;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_bw_cpu(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& weight
)
{
    const auto B = input.size(0);
    const auto Cin = input.size(1);
    const auto Cout = weight.size(1);

    const auto input_grad = torch::zeros_like(input);
    const auto weight_grad = torch::zeros({B, Cin, Cout}, weight.options());

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        auto grad_a = grad.accessor<scalar_t, 5>();
        auto input_a = input.accessor<scalar_t, 5>();
        auto weight_a = weight.accessor<scalar_t, 2>();
        auto input_grad_a = input_grad.accessor<scalar_t, 5>();
        auto weight_grad_a = weight_grad.accessor<scalar_t, 3>();

        linear_bw_cpu_impl<scalar_t>(
            grad_a, input_a, weight_a, input_grad_a, weight_grad_a);
    });

    return std::make_tuple(input_grad, weight_grad.sum({0}));
}

} // namespace m2
} // namespace lietorch