#pragma once
#ifndef LIETORCH_CPU_M2_INTERPOLATION_CPU_H
#define LIETORCH_CPU_M2_INTERPOLATION_CPU_H
/**
 * @file cpu/m2_interpolation_cpu.h
 *
 * @brief
 */

#define _USE_MATH_DEFINES
#include <cmath>

#include "../torch_include.h"

#include "../m2_element.h"

namespace lietorch
{
namespace m2
{

/**
 *  @brief Sample a point using nearest-neighbour interpolation, specified in
 * array coordinates or_co, y_co and x_co, on the M2 manifold by interpolating
 * the 3D tensor input_a.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param or_co orientation coordinate
 *  @param y_co y coordinate
 *  @param x_co x coordinate
 *
 *
 */
template <typename scalar_t = float, typename coordinate_t = float>
inline scalar_t
interpolate_nearest_non_periodic(
    const torch::TensorAccessor<scalar_t, 3>& input_a,
    const coordinate_t or_co,
    const coordinate_t y_co,
    const coordinate_t x_co,
    const scalar_t boundary = 0)
{
    const int64_t ors = input_a.size(0);
    const int64_t h = input_a.size(1);
    const int64_t w = input_a.size(2);

    const int64_t i = std::lround(or_co);
    const int64_t j = std::lround(y_co);
    const int64_t k = std::lround(x_co);

    if (i < 0 || i >= ors || j < 0 || j >= h || k < 0 || k >= w)
    {
        return boundary;
    }

    return input_a[i][j][k];
}

/**
 *  @brief Sample a point using trilinear interpolation, specified in array
 * coordinates or_co, y_co and x_co, on the M2 manifold by interpolating the 3D
 * tensor input_a.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param or_co orientation coordinate
 *  @param y_co y coordinate
 *  @param x_co x coordinate
 *
 *
 */
template <typename scalar_t = float, typename coordinate_t = float>
inline scalar_t
interpolate_trilinear(
    const torch::TensorAccessor<scalar_t, 3>& input_a,
    const coordinate_t or_co,
    const coordinate_t y_co,
    const coordinate_t x_co,
    const scalar_t boundary = 0)
{
    const int64_t ors = input_a.size(0);
    const int64_t h = input_a.size(1);
    const int64_t w = input_a.size(2);

    if (y_co <= -1 || y_co >= h || x_co <= -1 || x_co >= w)
    {
        return boundary;
    }

    const coordinate_t or_co_mod =
        fmod(or_co, static_cast<coordinate_t>(ors)) >= 0
            ? static_cast<coordinate_t>(
                  fmod(or_co, static_cast<coordinate_t>(ors)))
            : static_cast<coordinate_t>(
                  fmod(or_co, static_cast<coordinate_t>(ors)) +
                  static_cast<coordinate_t>(ors));

    const int64_t i0 = static_cast<int64_t>(floor(or_co_mod));
    const int64_t i1 = (i0 + 1) % ors;
    const int64_t j = static_cast<int64_t>(floor(y_co));
    const int64_t k = static_cast<int64_t>(floor(x_co));

    const scalar_t or_co_rem_signed = static_cast<scalar_t>(fmod(or_co, 1));
    const scalar_t y_co_rem_signed = static_cast<scalar_t>(fmod(y_co, 1));
    const scalar_t x_co_rem_signed = static_cast<scalar_t>(fmod(x_co, 1));

    const scalar_t or_co_rem =
        or_co_rem_signed >= 0 ? or_co_rem_signed : or_co_rem_signed + 1;
    const scalar_t y_co_rem =
        y_co_rem_signed >= 0 ? y_co_rem_signed : y_co_rem_signed + 1;
    const scalar_t x_co_rem =
        x_co_rem_signed >= 0 ? x_co_rem_signed : x_co_rem_signed + 1;

    const scalar_t v000 = (j < 0 || k < 0) ? boundary : input_a[i0][j][k];
    const scalar_t v100 = (j < 0 || k < 0) ? boundary : input_a[i1][j][k];
    const scalar_t v010 =
        (j >= h - 1 || k < 0) ? boundary : input_a[i0][j + 1][k];
    const scalar_t v110 =
        (j >= h - 1 || k < 0) ? boundary : input_a[i1][j + 1][k];
    const scalar_t v001 =
        (j < 0 || k >= w - 1) ? boundary : input_a[i0][j][k + 1];
    const scalar_t v101 =
        (j < 0 || k >= w - 1) ? boundary : input_a[i1][j][k + 1];
    const scalar_t v011 =
        (j >= h - 1 || k >= w - 1) ? boundary : input_a[i0][j + 1][k + 1];
    const scalar_t v111 =
        (j >= h - 1 || k >= w - 1) ? boundary : input_a[i1][j + 1][k + 1];

    const scalar_t v00 = (1 - or_co_rem) * v000 + or_co_rem * v100;
    const scalar_t v10 = (1 - or_co_rem) * v010 + or_co_rem * v110;
    const scalar_t v01 = (1 - or_co_rem) * v001 + or_co_rem * v101;
    const scalar_t v11 = (1 - or_co_rem) * v011 + or_co_rem * v111;

    const scalar_t v0 = (1 - y_co_rem) * v00 + y_co_rem * v10;
    const scalar_t v1 = (1 - y_co_rem) * v01 + y_co_rem * v11;

    const scalar_t v = (1 - x_co_rem) * v0 + x_co_rem * v1;

    return v;
}

/**
 *  @brief Sample a point using trilinear interpolation, specified by an
 * m2::element, on the M2 manifold by interpolating the 3D tensor input_a.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates used by g
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param g

 *
 *
 */
template <typename scalar_t = float, typename coordinate_t = float>
inline scalar_t
interpolate_trilinear(
    const torch::TensorAccessor<scalar_t, 3>& input_a,
    const m2::element<coordinate_t>& g,
    const scalar_t boundary = 0)
{
    const coordinate_t ors = static_cast<coordinate_t>(input_a.size(0));
    const coordinate_t or_co = ors * g.t / g.t_scale;
    return interpolate_trilinear<scalar_t, coordinate_t>(
        input_a, or_co, g.y, g.x, boundary);
}

/**
 *  @brief Sample a point using trilinear interpolation, specified in array
 * coordinates or_co, y_co and x_co, on the M2 manifold by interpolating the 3D
 * tensor input_a, additionally returns the gradient at the sampled point.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param or_co orientation coordinate
 *  @param y_co y coordinate
 *  @param x_co x coordinate
 *
 *
 */
template <typename scalar_t = float, typename coordinate_t = float>
inline std::tuple<scalar_t, scalar_t, scalar_t, scalar_t>
interpolate_trilinear_with_grad(
    const torch::TensorAccessor<scalar_t, 3>& input_a,
    const coordinate_t or_co,
    const coordinate_t y_co,
    const coordinate_t x_co,
    const scalar_t boundary = 0)
{
    constexpr scalar_t PI = static_cast<scalar_t>(M_PI);
    constexpr scalar_t ZERO = static_cast<scalar_t>(0);

    const int64_t ors = input_a.size(0);
    const int64_t h = input_a.size(1);
    const int64_t w = input_a.size(2);

    if (y_co <= -1 || y_co >= h || x_co <= -1 || x_co >= w)
    {
        return {boundary, ZERO, ZERO, ZERO};
    }

    const coordinate_t or_co_mod =
        fmod(or_co, static_cast<coordinate_t>(ors)) >= 0
            ? static_cast<coordinate_t>(
                  fmod(or_co, static_cast<coordinate_t>(ors)))
            : static_cast<coordinate_t>(
                  fmod(or_co, static_cast<coordinate_t>(ors)) +
                  static_cast<coordinate_t>(ors));

    const int64_t i0 = static_cast<int64_t>(floor(or_co_mod));
    const int64_t i1 = (i0 + 1) % ors;
    const int64_t j = static_cast<int64_t>(floor(y_co));
    const int64_t k = static_cast<int64_t>(floor(x_co));

    const scalar_t or_co_rem_signed = static_cast<scalar_t>(fmod(or_co, 1));
    const scalar_t y_co_rem_signed = static_cast<scalar_t>(fmod(y_co, 1));
    const scalar_t x_co_rem_signed = static_cast<scalar_t>(fmod(x_co, 1));

    const scalar_t or_co_rem =
        or_co_rem_signed >= 0 ? or_co_rem_signed : or_co_rem_signed + 1;
    const scalar_t y_co_rem =
        y_co_rem_signed >= 0 ? y_co_rem_signed : y_co_rem_signed + 1;
    const scalar_t x_co_rem =
        x_co_rem_signed >= 0 ? x_co_rem_signed : x_co_rem_signed + 1;

    const scalar_t v000 = (j < 0 || k < 0) ? boundary : input_a[i0][j][k];
    const scalar_t v100 = (j < 0 || k < 0) ? boundary : input_a[i1][j][k];
    const scalar_t v010 =
        (j >= h - 1 || k < 0) ? boundary : input_a[i0][j + 1][k];
    const scalar_t v110 =
        (j >= h - 1 || k < 0) ? boundary : input_a[i1][j + 1][k];
    const scalar_t v001 =
        (j < 0 || k >= w - 1) ? boundary : input_a[i0][j][k + 1];
    const scalar_t v101 =
        (j < 0 || k >= w - 1) ? boundary : input_a[i1][j][k + 1];
    const scalar_t v011 =
        (j >= h - 1 || k >= w - 1) ? boundary : input_a[i0][j + 1][k + 1];
    const scalar_t v111 =
        (j >= h - 1 || k >= w - 1) ? boundary : input_a[i1][j + 1][k + 1];

    const scalar_t v00 = (1 - or_co_rem) * v000 + or_co_rem * v100;
    const scalar_t v10 = (1 - or_co_rem) * v010 + or_co_rem * v110;
    const scalar_t v01 = (1 - or_co_rem) * v001 + or_co_rem * v101;
    const scalar_t v11 = (1 - or_co_rem) * v011 + or_co_rem * v111;

    const scalar_t v0 = (1 - y_co_rem) * v00 + y_co_rem * v10;
    const scalar_t v1 = (1 - y_co_rem) * v01 + y_co_rem * v11;

    const scalar_t v = (1 - x_co_rem) * v0 + x_co_rem * v1;

    const scalar_t dx = v1 - v0;
    const scalar_t dy = (1 - x_co_rem) * (v10 - v00) + x_co_rem * (v11 - v01);

    const scalar_t v0dz =
        (1 - y_co_rem) * (v100 - v000) + y_co_rem * (v110 - v010);
    const scalar_t v1dz =
        (1 - y_co_rem) * (v101 - v001) + y_co_rem * (v111 - v011);
    const scalar_t dz = (1 - x_co_rem) * v0dz + x_co_rem * v1dz;

    return {v, dz, dy, dx};
}

/**
 *  @brief Sample a point using trilinear interpolation, specified by a
 * m2::element, on the M2 manifold by interpolating the 3D tensor input_a,
 * additionally returns the gradient at the sampled point.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates used by g
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param g group element at which to sample.
 *
 *
 */
template <typename scalar_t = float, typename coordinate_t = float>
inline std::tuple<scalar_t, scalar_t, scalar_t, scalar_t>
interpolate_trilinear_with_grad(
    const torch::TensorAccessor<scalar_t, 3>& input_a,
    const m2::element<coordinate_t>& g,
    const scalar_t boundary = 0)
{
    const coordinate_t ors = static_cast<coordinate_t>(input_a.size(0));
    const coordinate_t or_co = ors * g.t / g.t_scale;
    return interpolate_trilinear_with_grad<scalar_t, coordinate_t>(
        input_a, or_co, g.y, g.x, boundary);
}

} // namespace m2
} // namespace lietorch

#endif