#pragma once
#ifndef LIETORCH_TEMPERATE_CPU_H
#define LIETORCH_TEMPERATE_CPU_H

/** @file actmap_cpu.h
 *  @brief Activation map operators
 *
 */

#include "../torch_include.h"

namespace lietorch
{
namespace temperate
{

using torch::Tensor;

/**
 * @brief Multiplication in the `Act` semimodule.
 *
 * @param a Tensor of shape `[...]`
 * @param b Tensor of shape `[...]`
 *
 * @return a ⊙ b
 */
Tensor
mul_cpu(const Tensor& a, const Tensor& b);

/**
 * @brief Addition in the `Act` semimodule.
 *
 * @param a Tensor of shape `[...]`
 * @param b Tensor of shape `[...]`
 *
 * @return a ⊕ b
 */
Tensor
plus_cpu(const Tensor& a, const Tensor& b);

uint8_t
mul_lut_u8(uint8_t x, uint8_t y);

} // namespace temperate
} // namespace lietorch

#endif // LIETORCH_TEMPERATE_H