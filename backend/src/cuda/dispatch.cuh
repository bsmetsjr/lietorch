#pragma once
#ifndef LIETORCH_CUDA_DISPATCH_H
#define LIETORCH_CUDA_DISPATCH_H

/** @file cuda/dispatch.cuh
 *
 *  @brief Macros and inline functions to help with dispatching/launching CUDA
 * kernels that operate on tensors.
 *
 *
 *
 */

#include "../vformat.h"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <iostream>
#include <stdexcept>

#include "../torch_include.h"

#if !defined(__CUDACC__)
#define RestrictPtrTraits DefaultPtrTraits
#endif

#define LT_THREADS_PER_BLOCKS 512
#define LT_BLOCKS_FOR_ELEMENTS(N)                                              \
    (N + LT_THREADS_PER_BLOCKS - 1) / LT_THREADS_PER_BLOCKS

namespace lietorch
{

/** @brief Compute global thread id assuming the 1D grid of 1D blocks addressing
 * scheme, i.e. we always let blockDim.y = blockDim.z = gridDim.y = gridDim.z
 * = 1. We use 32-bit integers for efficient pointer arithmetic on the GPU, this
 * does limit the maximum size of a tensor to 2^32 elements.
 *
 */
__device__ __forceinline__ uint32_t
linear_idx()
{
    return blockIdx.x * blockDim.x + threadIdx.x;
}

/**
 * @brief Determine the needed thread-block size (dim3) to launch a thread for
 * every element of this tensor assuming the 1D grid of 1D blocks addressing
 * scheme.
 */
__host__ __forceinline__ dim3
threads(const torch::Tensor& a)
{
    if (a.numel() < LT_THREADS_PER_BLOCKS)
    {
        return dim3(static_cast<unsigned int>(a.numel()));
    }
    else
    {
        return dim3(LT_THREADS_PER_BLOCKS);
    }
}

/**
 * @brief Determine the needed grid size (dim3) to launch a thread for every
 * element of this tensor assuming the 1D grid of 1D blocks addressing scheme.
 */
__host__ __forceinline__ dim3
blocks(const torch::Tensor& a)
{
    return dim3(LT_BLOCKS_FOR_ELEMENTS(static_cast<unsigned int>(a.numel())));
}

/**
 *
 */

/**
 * @brief Wrapper for CUDA API calls that provides error checking.
 */
#define CUDA_CALL(call) _cuda_call((call), __FILE__, __LINE__)

// CUDA API wrapper
inline __host__ cudaError_t
_cuda_call(
    cudaError_t code, const char* file, const int line, const bool abort = true)
{
    if (code != cudaSuccess)
    {

        auto msg = vformat(
            "GPU Error: %s in %s:%d\n", cudaGetErrorName(code), file, line);
        std::cerr << msg << std::endl;
        if (abort)
        {
            throw std::runtime_error(msg);
        }
    }
    return code;
}

} // namespace lietorch

#endif