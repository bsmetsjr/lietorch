#include "m2_cuda.cuh"

#pragma warning(push, 1)
#include <ATen/ATen.h>
#include <ATen/cuda/NumericLimits.cuh>
#pragma warning(pop)

#include "../m2_element.h"
#include "atomics.cuh"
#include "dispatch.cuh"
#include "m2_interpolation_cuda.cuh"
#include "math_cuda.cuh"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_constants.h>

#include "../dbg.h"

namespace // internal namespace
{

using namespace ::lietorch;

// These are used to map C++ types to ScalarTypes.
// This is copied from
// https://github.com/pytorch/pytorch/blob/master/c10/core/ScalarType.h (master
// @ c070e8f) on 9/04/2020 as it is needed here but is not available in
// pytorch 1.4.0.
// TODO: remove when it comes included in the pytorch release version.

template <typename> struct CPPTypeToScalarType
{
    constexpr static c10::ScalarType value = c10::ScalarType::Undefined;
};

#define SPECIALIZE_CPPTypeToScalarType(cpp_type, scalar_type)                  \
    template <> struct CPPTypeToScalarType<cpp_type>                           \
    {                                                                          \
        constexpr static c10::ScalarType value = c10::ScalarType::scalar_type; \
    };

AT_FORALL_SCALAR_TYPES_WITH_COMPLEX_AND_QINTS(SPECIALIZE_CPPTypeToScalarType)

#undef SPECIALIZE_CPPTypeToScalarType

/*

*/
template <typename scalar_t>
inline torch::Tensor
adp_filter(
    const int64_t orientations,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    const int r =
        static_cast<int>(std::floor(std::fmax(longitudinal, lateral) / 2));

    const torch::ScalarType t = CPPTypeToScalarType<scalar_t>::value;
    const auto kernel = torch::empty(
        {orientations, 2 * r + 1, 2 * r + 1}, torch::TensorOptions().dtype(t));
    auto kernel_a = kernel.accessor<scalar_t, 3>();

    const double p = 2 * alpha / (2 * alpha - 1);

    for (int i = 0; i < orientations; i++)
    {
        const double theta = i * M_PI / orientations;
        const double cos_theta = std::cos(theta);
        const double sin_theta = std::sin(theta);

        for (int j = 0; j < 2 * r + 1; j++)
        {
            const double y = static_cast<double>(j - r);

            for (int k = 0; k < 2 * r + 1; k++)
            {
                const double x = static_cast<double>(k - r);
                const double rx =
                    2 * (x * cos_theta + y * sin_theta) / longitudinal;
                const double ry = 2 * (y * cos_theta - x * sin_theta) / lateral;

                const double norm =
                    std::sqrt(std::pow(rx, 2) + std::pow(ry, 2));
                kernel_a[i][j][k] =
                    static_cast<scalar_t>(-scale * std::pow(norm, p));
            }
        }
    }

    return kernel;
}

/*

*/
template <typename scalar_t>
__global__ void
adp_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_a,
    const torch::PackedTensorAccessor32<scalar_t, 3, torch::RestrictPtrTraits>
        filter_a,
    torch::PackedTensorAccessor32<int64_t, 5, torch::RestrictPtrTraits>
        backindex_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t ors = input_a.size(2);
    const int32_t h = input_a.size(3);
    const int32_t w = input_a.size(4);

    const int32_t batch = thread_idx / out_a.stride(0);
    const int32_t channel = (thread_idx % out_a.stride(0)) / out_a.stride(1);
    const int32_t i = (thread_idx % out_a.stride(1)) / out_a.stride(2);
    const int32_t j = thread_idx % out_a.stride(2);
    const int32_t r = (filter_a.size(1) - 1) / 2;

    if (batch >= out_a.size(0))
    {
        return;
    }

    scalar_t out_val = at::numeric_limits<scalar_t>::lowest();
    int32_t back_or = 0, back_h = 0, back_w = 0;

    for (int32_t k = -min(r, i); k <= min(h - i - 1, r); k++)
    {
        for (int32_t l = -min(r, j); l <= min(w - j - 1, r); l++)
        {
            for (int32_t m = 0; m < ors; m++)
            {
                const scalar_t new_val =
                    max(out_val,
                        input_a[batch][channel][m][i + k][j + l] +
                            filter_a[m][k + r][l + r]);

                if (new_val != out_val)
                {
                    out_val = new_val;
                    back_or = m;
                    back_h = i + k;
                    back_w = j + l;
                }
            }
        }
    }

    out_a[batch][channel][i][j] = out_val;
    backindex_a[batch][channel][i][j][0] = back_or;
    backindex_a[batch][channel][i][j][1] = back_h;
    backindex_a[batch][channel][i][j][2] = back_w;
}

/*

*/
template <typename scalar_t>
__global__ void
adp_bw_kernel(
    const torch::PackedTensorAccessor32<int64_t, 5, torch::RestrictPtrTraits>
        backindex_a,
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_grad_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t ors = input_grad_a.size(2);
    const int32_t h = input_grad_a.size(3);
    const int32_t w = input_grad_a.size(4);

    const int32_t batch = thread_idx / grad_a.stride(0);
    const int32_t channel = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t i = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t j = thread_idx % grad_a.stride(2);

    if (batch >= grad_a.size(0))
    {
        return;
    }

    const int32_t back_or = backindex_a[batch][channel][i][j][0];
    const int32_t back_h = backindex_a[batch][channel][i][j][1];
    const int32_t back_w = backindex_a[batch][channel][i][j][2];

    atomicAdd(
        &input_grad_a[batch][channel][back_or][back_h][back_w],
        grad_a[batch][channel][i][j]);
}

/*

*/
template <typename scalar_t, typename coordinate_t>
__global__ void
convection_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_a,
    const torch::
        PackedTensorAccessor32<coordinate_t, 2, torch::RestrictPtrTraits> g0_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> out_a,
    torch::PackedTensorAccessor32<scalar_t, 6, torch::RestrictPtrTraits>
        out_grad_field_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t ors = input_a.size(2);
    const int32_t h = input_a.size(3);
    const int32_t w = input_a.size(4);

    const coordinate_t ors_co = static_cast<coordinate_t>(ors);

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t i = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t j = (thread_idx % input_a.stride(2)) / input_a.stride(3);
    const int32_t k = thread_idx % input_a.stride(3);

    if (b >= input_a.size(0))
    {
        return;
    }

    using element = ::lietorch::m2::element<coordinate_t>;

    const element g0(g0_a[c][0], g0_a[c][1], g0_a[c][2], ors_co);
    const element gp(
        static_cast<coordinate_t>(i),
        static_cast<coordinate_t>(j),
        static_cast<coordinate_t>(k),
        ors_co);
    const element sample = gp * g0.inv();

    using namespace ::lietorch::m2;
    const auto v_dz_dy_dx =
        interpolate_m2_trilinear_with_grad_cuda<scalar_t, coordinate_t>(
            input_a[b][c], sample);

    const auto v = std::get<0>(v_dz_dy_dx);
    const auto dz = std::get<1>(v_dz_dy_dx);
    const auto dy = std::get<2>(v_dz_dy_dx);
    const auto dx = std::get<3>(v_dz_dy_dx);

    out_a[b][c][i][j][k] = v;
    out_grad_field_a[b][c][i][j][k][0] = dz;
    out_grad_field_a[b][c][i][j][k][1] = dy;
    out_grad_field_a[b][c][i][j][k][2] = dx;
}

/*

*/
template <typename scalar_t, typename coordinate_t>
__global__ void
convection_fw_no_grad_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_a,
    const torch::
        PackedTensorAccessor32<coordinate_t, 2, torch::RestrictPtrTraits> g0_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t ors = input_a.size(2);
    const int32_t h = input_a.size(3);
    const int32_t w = input_a.size(4);

    const coordinate_t ors_co = static_cast<coordinate_t>(ors);

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t i = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t j = (thread_idx % input_a.stride(2)) / input_a.stride(3);
    const int32_t k = thread_idx % input_a.stride(3);

    if (b >= input_a.size(0))
    {
        return;
    }

    using element = ::lietorch::m2::element<coordinate_t>;

    const element g0(g0_a[c][0], g0_a[c][1], g0_a[c][2], ors_co);
    const element gp(
        static_cast<coordinate_t>(i),
        static_cast<coordinate_t>(j),
        static_cast<coordinate_t>(k),
        ors_co);
    const element sample = gp * g0.inv();

    using namespace ::lietorch::m2;
    const auto v = interpolate_m2_trilinear_cuda<scalar_t, coordinate_t>(
        input_a[b][c], sample);

    out_a[b][c][i][j][k] = v;
}

/*

*/
template <typename scalar_t, typename coordinate_t>
__global__ void
convection_bw_kernel(
    const torch::
        PackedTensorAccessor32<coordinate_t, 2, torch::RestrictPtrTraits> g0_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_grad_a,
    const torch::PackedTensorAccessor32<scalar_t, 6, torch::RestrictPtrTraits>
        out_grad_field_a,
    torch::PackedTensorAccessor32<coordinate_t, 6, torch::RestrictPtrTraits>
        g0_splits_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t ors = grad_a.size(2);
    const int32_t h = grad_a.size(3);
    const int32_t w = grad_a.size(4);

    const coordinate_t ors_co = static_cast<coordinate_t>(ors);

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t i = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t j = (thread_idx % grad_a.stride(2)) / grad_a.stride(3);
    const int32_t k = thread_idx % grad_a.stride(3);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const coordinate_t z0 = g0_a[c][0];
    const coordinate_t y0 = g0_a[c][1];
    const coordinate_t x0 = g0_a[c][2];

    const auto cossin_theta0 = lt_cu_cossinpi<coordinate_t>(2 * z0 / ors_co);
    const coordinate_t cos_theta0 = std::get<0>(cossin_theta0);
    const coordinate_t sin_theta0 = std::get<1>(cossin_theta0);

    const coordinate_t a1 = -cos_theta0 * x0 - sin_theta0 * y0;
    const coordinate_t a2 = sin_theta0 * x0 - cos_theta0 * y0;

    const auto cossin_theta = lt_cu_cossinpi<coordinate_t>(2 * i / ors_co);
    const coordinate_t cos_theta = std::get<0>(cossin_theta);
    const coordinate_t sin_theta = std::get<1>(cossin_theta);

    const coordinate_t j00 = -1;
    const coordinate_t j01 = (2 * LT_CU_PI<coordinate_t>() / ors_co) *
                             (sin_theta * a2 - cos_theta * a1);
    const coordinate_t j02 = (2 * LT_CU_PI<coordinate_t>() / ors_co) *
                             (cos_theta * a2 + sin_theta * a1);

    const coordinate_t j10 = 0;
    const coordinate_t j11 = -sin_theta * sin_theta0 - cos_theta * cos_theta0;
    const coordinate_t j12 = -cos_theta * sin_theta0 + sin_theta * cos_theta0;

    const coordinate_t j20 = 0;
    const coordinate_t j21 = -sin_theta * cos_theta0 + cos_theta * sin_theta0;
    const coordinate_t j22 = -cos_theta * cos_theta0 - sin_theta * sin_theta0;

    const coordinate_t sample_i = lt_cu_fmod<coordinate_t>(i - z0, ors_co);
    const coordinate_t sample_j =
        static_cast<coordinate_t>(j) + sin_theta * a1 + cos_theta * a2;
    const coordinate_t sample_k =
        static_cast<coordinate_t>(k) + cos_theta * a1 - sin_theta * a2;

    if (sample_j <= -1 || sample_j >= h || sample_k <= -1 || sample_k >= w)
    {
        return;
    }

    const auto base_offset_i = lt_cu_intfrac<coordinate_t>(sample_i);
    const int32_t base_i = static_cast<int32_t>(std::get<0>(base_offset_i));
    const int32_t base_i0 = base_i >= 0 ? base_i : base_i + ors;
    const int32_t base_i1 = (base_i0 + 1) % ors;
    const scalar_t offset_i = static_cast<scalar_t>(std::get<1>(base_offset_i));

    const auto base_offset_j = lt_cu_intfrac<coordinate_t>(sample_j);
    const int32_t base_j = static_cast<int32_t>(std::get<0>(base_offset_j));
    const scalar_t offset_j = static_cast<scalar_t>(std::get<1>(base_offset_j));

    const auto base_offset_k = lt_cu_intfrac<coordinate_t>(sample_k);
    const int32_t base_k = static_cast<int32_t>(std::get<0>(base_offset_k));
    const scalar_t offset_k = static_cast<scalar_t>(std::get<1>(base_offset_k));

    const scalar_t w000 = (1 - offset_i) * (1 - offset_j) * (1 - offset_k);
    const scalar_t w100 = offset_i * (1 - offset_j) * (1 - offset_k);
    const scalar_t w010 = (1 - offset_i) * offset_j * (1 - offset_k);
    const scalar_t w110 = offset_i * offset_j * (1 - offset_k);
    const scalar_t w001 = (1 - offset_i) * (1 - offset_j) * offset_k;
    const scalar_t w101 = offset_i * (1 - offset_j) * offset_k;
    const scalar_t w011 = (1 - offset_i) * offset_j * offset_k;
    const scalar_t w111 = offset_i * offset_j * offset_k;

    const auto gval = grad_a[b][c][i][j][k];

    if (base_k >= 0)
    {
        if (base_j >= 0)
        {
            atomicAdd(
                &input_grad_a[b][c][base_i0][base_j][base_k], w000 * gval);
            atomicAdd(
                &input_grad_a[b][c][base_i1][base_j][base_k], w100 * gval);
        }
        if (base_j + 1 < h)
        {
            atomicAdd(
                &input_grad_a[b][c][base_i0][base_j + 1][base_k], w010 * gval);
            atomicAdd(
                &input_grad_a[b][c][base_i1][base_j + 1][base_k], w110 * gval);
        }
    }
    if (base_k + 1 < w)
    {
        if (base_j >= 0)
        {
            atomicAdd(
                &input_grad_a[b][c][base_i0][base_j][base_k + 1], w001 * gval);
            atomicAdd(
                &input_grad_a[b][c][base_i1][base_j][base_k + 1], w101 * gval);
        }
        if (base_j + 1 < h)
        {
            atomicAdd(
                &input_grad_a[b][c][base_i0][base_j + 1][base_k + 1],
                w011 * gval);
            atomicAdd(
                &input_grad_a[b][c][base_i1][base_j + 1][base_k + 1],
                w111 * gval);
        }
    }

    const coordinate_t dz =
        static_cast<coordinate_t>(out_grad_field_a[b][c][i][j][k][0]);
    const coordinate_t dy =
        static_cast<coordinate_t>(out_grad_field_a[b][c][i][j][k][1]);
    const coordinate_t dx =
        static_cast<coordinate_t>(out_grad_field_a[b][c][i][j][k][2]);
    const coordinate_t gval_co = static_cast<coordinate_t>(gval);

    g0_splits_a[b][c][i][j][k][0] = gval_co * (j00 * dz + j01 * dy + j02 * dx);
    g0_splits_a[b][c][i][j][k][1] = gval_co * (j10 * dz + j11 * dy + j12 * dx);
    g0_splits_a[b][c][i][j][k][2] = gval_co * (j20 * dz + j21 * dy + j22 * dx);
}

/*


*/
template <typename scalar_t>
__global__ void
rotated_kernel_stack_nearest_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> kernel_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kstack_a,
    const scalar_t boundary)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int32_t thread_idx = linear_idx();

    const int32_t ori = thread_idx / kstack_a.stride(0);
    const int32_t c = (thread_idx % kstack_a.stride(0)) / kstack_a.stride(1);
    const int32_t i = (thread_idx % kstack_a.stride(1)) / kstack_a.stride(2);
    const int32_t j = (thread_idx % kstack_a.stride(2)) / kstack_a.stride(3);
    const int32_t k = thread_idx % kstack_a.stride(3);

    if (ori >= kstack_a.size(0))
    {
        return;
    }

    const int32_t ker_ors = kernel_a.size(1);
    const int32_t ker_h = kernel_a.size(2);
    const int32_t ker_w = kernel_a.size(3);

    const int32_t ors = kstack_a.size(0);
    const int32_t cs = kstack_a.size(1);
    const int32_t stack_ors = kstack_a.size(2);
    const int32_t stack_h = kstack_a.size(3);
    const int32_t stack_w = kstack_a.size(4);

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / static_cast<co_t>(2.0);
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / static_cast<co_t>(2.0);
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / static_cast<co_t>(2.0);

    const element g0(0.0f, offset_y, offset_x);
    const co_t theta = 2.0f * LT_CU_PI<co_t>() * static_cast<co_t>(ori) /
                       static_cast<co_t>(ors);
    const element gtheta(theta, 0.0f, 0.0f); // rotate
    // shift center to origin, rotate and shift back.
    const element grot = g0 * gtheta.inv() * g0.inv();

    const co_t i_sample =
        static_cast<co_t>(i * ker_ors) / static_cast<co_t>(stack_ors);
    const co_t j_sample = static_cast<co_t>(j * ker_h) / static_cast<co_t>(stack_h);
    const co_t k_sample = static_cast<co_t>(k * ker_w) / static_cast<co_t>(stack_w);

    const element gker(i_sample, j_sample, k_sample, static_cast<co_t>(ker_ors));
    const element gsample = grot * gker;

    const int32_t ker_i = lround(i_sample);
    const int32_t ker_j = lround(gsample.y);
    const int32_t ker_k = lround(gsample.x);

    if (ker_i >= 0 && ker_i < ker_ors && ker_j >= 0 && ker_j < ker_h && ker_k >= 0 &&
        ker_k < ker_w)
    {

        kstack_a[ori][c][i][j][k] = kernel_a[c][ker_i][ker_j][ker_k];
    }
    else
    {
        kstack_a[ori][c][i][j][k] = boundary;
    }
}

/*

*/
template <typename scalar_t>
__global__ void
linear_convolution_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kstack_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> out_a
)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t i = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t j = (thread_idx % input_a.stride(2)) / input_a.stride(3);
    const int32_t k = thread_idx % input_a.stride(3);

    if (b >= input_a.size(0))
    {
        return;
    }

    const int32_t ors = input_a.size(2); // number of orientations
    const int32_t h = input_a.size(3);   // number of y's
    const int32_t w = input_a.size(4);   // number of x's

    const int32_t stack_ors = kstack_a.size(2);
    const int32_t stack_h = kstack_a.size(3);
    const int32_t stack_w = kstack_a.size(4);

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    for (int32_t stack_i = 0; stack_i < stack_ors; stack_i++)
    {
        for (int32_t stack_j = 0; stack_j < stack_h; stack_j++)
        {
            for (int32_t stack_k = 0; stack_k < stack_w; stack_k++)
            {
                // positive modulus
                const int32_t i_in = ((i - r_ors + stack_i) % ors + ors) % ors;
                const int32_t j_in = j - r_h + stack_j;
                const int32_t k_in = k - r_w + stack_k;

                if(j_in < 0 || j_in >= h || k_in < 0 || k_in >= w) continue;

                const scalar_t in = input_a[b][c][i_in][j_in][k_in];
                const scalar_t ker = kstack_a[i][c][stack_i][stack_j][stack_k];

                out_a[b][c][i][j][k] += in * ker;
            }
        }
    }

}


/*

*/
template <typename scalar_t>
__global__ void
linear_convolution_bw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_a,
    const torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits> kernel_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kernel_grad_a)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t i = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t j = (thread_idx % grad_a.stride(2)) / grad_a.stride(3);
    const int32_t k = thread_idx % grad_a.stride(3);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const int32_t ors = grad_a.size(2);
    const int32_t h = grad_a.size(3);  
    const int32_t w = grad_a.size(4);  

    const int32_t ker_ors = kernel_grad_a.size(2);
    const int32_t ker_h = kernel_grad_a.size(3);
    const int32_t ker_w = kernel_grad_a.size(4);

    const int32_t stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const int32_t stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const int32_t stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / 2.0;
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / 2.0;
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / 2.0;

    const element g0(0.0, offset_y, offset_x);
    const co_t theta =
        2.0f * LT_CU_PI<co_t>() * static_cast<co_t>(i) / static_cast<co_t>(ors);
    const element gtheta(theta, 0.0f, 0.0f); // rotate
    // shift center to origin, rotate and shift back.
    const element grot = g0 * gtheta.inv() * g0.inv();

    const scalar_t gradval = grad_a[b][c][i][j][k];

    for (int64_t stack_i = 0; stack_i < stack_ors; stack_i++)
    {
        for (int64_t stack_j = 0; stack_j < stack_h; stack_j++)
        {
            for (int64_t stack_k = 0; stack_k < stack_w; stack_k++)
            {
                const int64_t i_in = ((i - r_ors + stack_i) % ors + ors) % ors;
                const int64_t j_in = j - r_h + stack_j;
                const int64_t k_in = k - r_w + stack_k;

                if(j_in < 0 || j_in >= h || k_in < 0 || k_in >= w) continue;

                const co_t i_sample = static_cast<co_t>(stack_i * ker_ors) / static_cast<co_t>(stack_ors);
                const co_t j_sample = static_cast<co_t>(stack_j * ker_h) / static_cast<co_t>(stack_h);
                const co_t k_sample = static_cast<co_t>(stack_k * ker_w) / static_cast<co_t>(stack_w);
                const element gker(i_sample, j_sample, k_sample, static_cast<co_t>(ker_ors));
                const element gsample = grot * gker;

                const int32_t ker_i = lround(i_sample);
                const int32_t ker_j = lround(gsample.y);
                const int32_t ker_k = lround(gsample.x);

                if (ker_i >= 0 && ker_i < ker_ors && 
                    ker_j >= 0 && ker_j < ker_h && 
                    ker_k >= 0 && ker_k < ker_w
                )
                {
                    const auto in = input_a[b][c][i_in][j_in][k_in];
                    const auto ker = kernel_a[c][ker_i][ker_j][ker_k];

                    // TODO : THIS IS BAD & VERY SLOW
                    atomicAdd(&input_grad_a[b][c][i_in][j_in][k_in], gradval * ker);

                    // TODO : THIS IS BAD & VERY SLOW
                    atomicAdd(&kernel_grad_a[b][c][ker_i][ker_j][ker_k], gradval * in);
                }
            }
        }
    }
}


/*

*/
template <typename scalar_t>
__global__ void
morphological_convolution_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kstack_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> out_a,
    torch::PackedTensorAccessor32<int32_t, 6, torch::RestrictPtrTraits> backindex_a
)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t i = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t j = (thread_idx % input_a.stride(2)) / input_a.stride(3);
    const int32_t k = thread_idx % input_a.stride(3);

    if (b >= input_a.size(0))
    {
        return;
    }

    const int32_t ors = input_a.size(2); // number of orientations
    const int32_t h = input_a.size(3);   // number of y's
    const int32_t w = input_a.size(4);   // number of x's

    const int32_t stack_ors = kstack_a.size(2);
    const int32_t stack_h = kstack_a.size(3);
    const int32_t stack_w = kstack_a.size(4);

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    scalar_t val = at::numeric_limits<scalar_t>::max();
    int32_t hit_i, hit_j, hit_k;

    for (int32_t stack_i = 0; stack_i < stack_ors; stack_i++)
    {
        for (int32_t stack_j = 0; stack_j < stack_h; stack_j++)
        {
            for (int32_t stack_k = 0; stack_k < stack_w; stack_k++)
            {
                // positive modulus
                const int32_t i_in = ((i - r_ors + stack_i) % ors + ors) % ors;
                const int32_t j_in = j - r_h + stack_j;
                const int32_t k_in = k - r_w + stack_k;

                if(j_in < 0 || j_in >= h || k_in < 0 || k_in >= w) continue;

                const scalar_t in = input_a[b][c][i_in][j_in][k_in];
                const scalar_t ker = kstack_a[i][c][stack_i][stack_j][stack_k];

                const scalar_t newval = in + ker;

                if (newval < val)
                {
                    val = newval;

                    hit_i = i - r_ors + stack_i;
                    hit_j = j_in;
                    hit_k = k_in;
                }
            }
        }
    }

    out_a[b][c][i][j][k] = val;
    backindex_a[b][c][i][j][k][0] = hit_i;
    backindex_a[b][c][i][j][k][1] = hit_j;
    backindex_a[b][c][i][j][k][2] = hit_k;
}

/*




*/
template <typename scalar_t>
__global__ void
morphological_convolution_fw_no_grad_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kstack_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t i = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t j = (thread_idx % input_a.stride(2)) / input_a.stride(3);
    const int32_t k = thread_idx % input_a.stride(3);

    if (b >= input_a.size(0))
    {
        return;
    }

    const int32_t ors = input_a.size(2); // number of orientations
    const int32_t h = input_a.size(3);   // number of y's
    const int32_t w = input_a.size(4);   // number of x's

    const int32_t stack_ors = kstack_a.size(2);
    const int32_t stack_h = kstack_a.size(3);
    const int32_t stack_w = kstack_a.size(4);

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    scalar_t val = at::numeric_limits<scalar_t>::max();
    int32_t hit_i, hit_j, hit_k;

    for (int32_t stack_i = 0; stack_i < stack_ors; stack_i++)
    {
        for (int32_t stack_j = 0; stack_j < stack_h; stack_j++)
        {
            for (int32_t stack_k = 0; stack_k < stack_w; stack_k++)
            {
                // positive modulus
                const int32_t i_in = ((i - r_ors + stack_i) % ors + ors) % ors;
                const int32_t j_in = j - r_h + stack_j;
                const int32_t k_in = k - r_w + stack_k;

                if(j_in < 0 || j_in >= h || k_in < 0 || k_in >= w) continue;

                const scalar_t in = input_a[b][c][i_in][j_in][k_in];
                const scalar_t ker = kstack_a[i][c][stack_i][stack_j][stack_k];

                const scalar_t newval = in + ker;
                if (newval < val)
                {
                    val = newval;
                }
            }
        }
    }

    out_a[b][c][i][j][k] = val;
}


template <typename scalar_t>
__global__ void
morphological_convolution_bw_input_grad_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> grad_a,
    const torch::PackedTensorAccessor32<int32_t, 6, torch::RestrictPtrTraits> backindex_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kernel_grad_a
)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t i_in = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t j_in = (thread_idx % grad_a.stride(2)) / grad_a.stride(3);
    const int32_t k_in = thread_idx % grad_a.stride(3);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const int32_t ors = grad_a.size(2);
    const int32_t h = grad_a.size(3);  
    const int32_t w = grad_a.size(4);  

    const int32_t ker_ors = kernel_grad_a.size(2);
    const int32_t ker_h = kernel_grad_a.size(3);
    const int32_t ker_w = kernel_grad_a.size(4);

    const int32_t stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const int32_t stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const int32_t stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    for (int32_t stack_i = 0; stack_i < stack_ors; stack_i++)
    {
        for (int32_t stack_j = 0; stack_j < stack_h; stack_j++)
        {
            for (int32_t stack_k = 0; stack_k < stack_w; stack_k++)
            {
                const int32_t i_out = ((i_in - r_ors + stack_i) % ors + ors) % ors;
                const int32_t j_out = j_in - r_h + stack_j;
                const int32_t k_out = k_in - r_h + stack_j;
                
                if(j_out < 0 || j_out >= h || k_out < 0 || k_out >= w) continue;

                const int32_t hit_i = backindex_a[b][c][i_out][j_out][k_out][0];
                const int32_t hit_j = backindex_a[b][c][i_out][j_out][k_out][1];
                const int32_t hit_k = backindex_a[b][c][i_out][j_out][k_out][2];
                const int32_t hit_i_mod = (hit_i % ors + ors) % ors;

                if(hit_i_mod == i_in && hit_j == j_in && hit_k == k_in)
                {
                    const scalar_t gradval = grad_a[b][c][i_out][j_out][k_out];
                    input_grad_a[b][c][i_in][j_in][k_in] += gradval;
                }

            }
        }
    }
}

template <typename scalar_t>
__global__ void
morphological_convolution_bw_kernel_grad_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> grad_a,
    const torch::PackedTensorAccessor32<int32_t, 6, torch::RestrictPtrTraits> backindex_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kernel_grad_a
)
{
using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t i = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t j = (thread_idx % grad_a.stride(2)) / grad_a.stride(3);
    const int32_t k = thread_idx % grad_a.stride(3);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const int32_t ors = grad_a.size(2);
    const int32_t h = grad_a.size(3);  
    const int32_t w = grad_a.size(4);  

    const int32_t ker_ors = kernel_grad_a.size(2);
    const int32_t ker_h = kernel_grad_a.size(3);
    const int32_t ker_w = kernel_grad_a.size(4);

    const int32_t stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const int32_t stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const int32_t stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / 2.0;
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / 2.0;
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / 2.0;

    const element g0(0.0, offset_y, offset_x);

    const co_t theta =
        2.0f * LT_CU_PI<co_t>() * static_cast<co_t>(i) / static_cast<co_t>(ors);
    const element gtheta(theta, 0.0f, 0.0f); // rotate
    // shift center to origin, rotate and shift back.
    const element grot = g0 * gtheta.inv() * g0.inv();

    const int32_t hit_i = backindex_a[b][c][i][j][k][0];
    const int32_t hit_j = backindex_a[b][c][i][j][k][1];
    const int32_t hit_k = backindex_a[b][c][i][j][k][2];
    const int32_t hit_i_mod = (hit_i % ors + ors) % ors;

    const scalar_t gradval = grad_a[b][c][i][j][k];

    const int32_t stack_i = hit_i - i + r_ors;
    const int32_t stack_j = hit_j - j + r_h;
    const int32_t stack_k = hit_k - k + r_w;

    const co_t i_sample =
        static_cast<co_t>(stack_i * ker_ors) / static_cast<co_t>(stack_ors);
    const co_t j_sample =
        static_cast<co_t>(stack_j * ker_h) / static_cast<co_t>(stack_h);
    const co_t k_sample =
        static_cast<co_t>(stack_k * ker_w) / static_cast<co_t>(stack_w);
    const element gker(i_sample, j_sample, k_sample, static_cast<co_t>(ker_ors));
    const element gsample = grot * gker;

    const int32_t ker_i = lround(i_sample);
    const int32_t ker_j = lround(gsample.y);
    const int32_t ker_k = lround(gsample.x);

    if (ker_i >= 0 && ker_i < ker_ors && ker_j >= 0 && ker_j < ker_h && ker_k >= 0 &&
        ker_k < ker_w)
    {
        // TODO : THIS IS BAD & VERY SLOW
        atomicAdd(&kernel_grad_a[b][c][ker_i][ker_j][ker_k], gradval);
    }
}


/*

*/
template <typename scalar_t>
__global__ void
morphological_convolution_bw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> grad_a,
    const torch::PackedTensorAccessor32<int32_t, 6, torch::RestrictPtrTraits> backindex_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> input_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> kernel_grad_a)
{
    using co_t = float;
    using element = ::lietorch::m2::element<co_t>;

    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / grad_a.stride(0);
    const int32_t c = (thread_idx % grad_a.stride(0)) / grad_a.stride(1);
    const int32_t i = (thread_idx % grad_a.stride(1)) / grad_a.stride(2);
    const int32_t j = (thread_idx % grad_a.stride(2)) / grad_a.stride(3);
    const int32_t k = thread_idx % grad_a.stride(3);

    if (b >= grad_a.size(0))
    {
        return;
    }

    const int32_t ors = grad_a.size(2);
    const int32_t h = grad_a.size(3);  
    const int32_t w = grad_a.size(4);  

    const int32_t ker_ors = kernel_grad_a.size(2);
    const int32_t ker_h = kernel_grad_a.size(3);
    const int32_t ker_w = kernel_grad_a.size(4);

    const int32_t stack_ors = ker_ors % 2 == 0 ? ker_ors + 1 : ker_ors;
    const int32_t stack_h = ker_h % 2 == 0 ? ker_h + 1 : ker_h;
    const int32_t stack_w = ker_w % 2 == 0 ? ker_w + 1 : ker_w;

    const int32_t r_ors = (stack_ors - 1) / 2;
    const int32_t r_h = (stack_h - 1) / 2;
    const int32_t r_w = (stack_w - 1) / 2;

    const co_t offset_t = static_cast<co_t>(ker_ors - 1) / 2.0;
    const co_t offset_y = static_cast<co_t>(ker_h - 1) / 2.0;
    const co_t offset_x = static_cast<co_t>(ker_w - 1) / 2.0;
    const element g0(0.0, offset_y, offset_x);
    const co_t theta =
        2.0f * LT_CU_PI<co_t>() * static_cast<co_t>(i) / static_cast<co_t>(ors);
    const element gtheta(theta, 0.0f, 0.0f); // rotate
    // shift center to origin, rotate and shift back.
    const element grot = g0 * gtheta.inv() * g0.inv();

    const int32_t hit_i = backindex_a[b][c][i][j][k][0];
    const int32_t hit_j = backindex_a[b][c][i][j][k][1];
    const int32_t hit_k = backindex_a[b][c][i][j][k][2];
    const int32_t hit_i_mod = (hit_i % ors + ors) % ors;

    const scalar_t gradval = grad_a[b][c][i][j][k];

    // TODO : THIS IS BAD & VERY SLOW
    atomicAdd(&input_grad_a[b][c][hit_i_mod][hit_j][hit_k], gradval);

    const int32_t stack_i = hit_i - i + r_ors;
    const int32_t stack_j = hit_j - j + r_h;
    const int32_t stack_k = hit_k - k + r_w;

    const co_t i_sample =
        static_cast<co_t>(stack_i * ker_ors) / static_cast<co_t>(stack_ors);
    const co_t j_sample =
        static_cast<co_t>(stack_j * ker_h) / static_cast<co_t>(stack_h);
    const co_t k_sample =
        static_cast<co_t>(stack_k * ker_w) / static_cast<co_t>(stack_w);
    const element gker(i_sample, j_sample, k_sample, static_cast<co_t>(ker_ors));
    const element gsample = grot * gker;

    const int32_t ker_i = lround(i_sample);
    const int32_t ker_j = lround(gsample.y);
    const int32_t ker_k = lround(gsample.x);

    if (ker_i >= 0 && ker_i < ker_ors && ker_j >= 0 && ker_j < ker_h && ker_k >= 0 &&
        ker_k < ker_w)
    {
        // TODO : THIS IS BAD & VERY SLOW
        atomicAdd(&kernel_grad_a[b][c][ker_i][ker_j][ker_k], gradval);
    }
}

/*




*/
template <typename scalar_t>
__global__ void
linear_fw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_a,
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        weight_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits> out_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / out_a.stride(0);
    const int32_t cout = (thread_idx % out_a.stride(0)) / out_a.stride(1);
    const int32_t i = (thread_idx % out_a.stride(1)) / out_a.stride(2);
    const int32_t j = (thread_idx % out_a.stride(2)) / out_a.stride(3);
    const int32_t k = thread_idx % out_a.stride(3);

    if (b >= out_a.size(0))
    {
        return;
    }

    for (int32_t cin = 0; cin < input_a.size(1); cin++)
    {
        out_a[b][cout][i][j][k] +=
            weight_a[cin][cout] * input_a[b][cin][i][j][k];
    }
}

/*




*/
template <typename scalar_t>
__global__ void
linear_bw_kernel(
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        grad_a,
    const torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_a,
    const torch::PackedTensorAccessor32<scalar_t, 2, torch::RestrictPtrTraits>
        weight_a,
    torch::PackedTensorAccessor32<scalar_t, 5, torch::RestrictPtrTraits>
        input_grad_a,
    torch::PackedTensorAccessor32<scalar_t, 4, torch::RestrictPtrTraits>
        weight_grad_a)
{
    const int32_t thread_idx = linear_idx();

    const int32_t b = thread_idx / input_a.stride(0);
    const int32_t c_in = (thread_idx % input_a.stride(0)) / input_a.stride(1);
    const int32_t i = (thread_idx % input_a.stride(1)) / input_a.stride(2);
    const int32_t j = (thread_idx % input_a.stride(2)) / input_a.stride(3);
    const int32_t k = thread_idx % input_a.stride(3);

    if (b >= input_a.size(0))
    {
        return;
    }

    scalar_t input_grad_val = static_cast<scalar_t>(0);
    for (int32_t c_out = 0; c_out < grad_a.size(1); c_out++)
    {
        input_grad_val += grad_a[b][c_out][i][j][k] * weight_a[c_in][c_out];
        scalar_t weight_grad_val =
            input_a[b][c_in][i][j][k] * grad_a[b][c_out][i][j][k];

        // TODO : THIS IS BAD & VERY SLOW
        atomicAdd(&weight_grad_a[b][i][c_in][c_out], weight_grad_val);
    }
    // TODO : THIS IS BAD & VERY SLOW
    atomicAdd(&input_grad_a[b][c_in][i][j][k], input_grad_val);
}

} // namespace

namespace lietorch
{
namespace m2
{

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
anisotropic_dilated_project_fw_cuda(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale)
{
    const auto input_size = input.sizes(); // [B,C,Or,H,W]
    const std::vector<int64_t> out_size{
        input_size[0],
        input_size[1],
        input_size[3],
        input_size[4]}; // [B,C,H,W]
    const std::vector<int64_t> backindex_size{
        input_size[0],
        input_size[1],
        input_size[3],
        input_size[4],
        3}; // [B,C,H,W,3]

    const auto out = torch::empty(out_size, input.options());
    const auto backindex = torch::empty(
        backindex_size,
        torch::TensorOptions().dtype(torch::kInt64).device(input.device()));

    const auto orientations = input.size(2);

    AT_DISPATCH_ALL_TYPES(input.scalar_type(), __func__, [&] {
        const auto cpu_filter = adp_filter<scalar_t>(
            orientations, longitudinal, lateral, alpha, scale);
        const auto cuda_filter = cpu_filter.to(input.device());

        auto input_a =
            input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto filter_a =
            cuda_filter
                .packed_accessor32<scalar_t, 3, torch::RestrictPtrTraits>();
        auto backindex_a =
            backindex.packed_accessor32<int64_t, 5, torch::RestrictPtrTraits>();
        auto out_a =
            out.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();

        void* args[] = {&input_a, &filter_a, &backindex_a, &out_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)adp_fw_kernel<scalar_t>, blocks(out), threads(out), args));
    });

    return std::make_tuple(out, backindex);
}

/*

*/
torch::Tensor
anisotropic_dilated_project_bw_cuda(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const torch::List<int64_t> input_shape)
{
    const auto input_grad = torch::zeros(
        {input_shape[0],
         input_shape[1],
         input_shape[2],
         input_shape[3],
         input_shape[4]},
        grad.options());

    AT_DISPATCH_ALL_TYPES(grad.scalar_type(), __func__, [&] {
        auto backindex_a = backindex.packed_accessor32<int64_t, 5, torch::RestrictPtrTraits>();
        auto grad_a = grad.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto input_grad_a = input_grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

        void* args[] = {&backindex_a, &grad_a, &input_grad_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)adp_bw_kernel<scalar_t>, blocks(grad), threads(grad), args));
    });

    return input_grad;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cuda(
    const torch::Tensor& input, // [B,C,Or,H,W]
    const torch::Tensor& g0     // [C,3]
)
{
    const auto out = torch::zeros_like(input);

    if (input.requires_grad() || g0.requires_grad())
    {
        std::vector<int64_t> field_size = input.sizes().vec(); // = {B,C,Or,H,W}
        field_size.push_back(3); // = {B,C,Or,H,W,3}
        const auto out_grad_field = input.new_zeros(field_size);

        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a =
                input
                    .packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto out_a =
                out.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto out_grad_field_a =
                out_grad_field
                    .packed_accessor32<scalar_t, 6, torch::RestrictPtrTraits>();

            switch (g0.scalar_type())
            {
            case torch::kFloat32:
            {
                auto g0_a =
                    g0.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a, &out_grad_field_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_kernel<scalar_t, float>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            case torch::kFloat64:
            {
                auto g0_a =
                    g0.packed_accessor32<double, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a, &out_grad_field_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_kernel<scalar_t, double>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            default:
                TORCH_CHECK(
                    false,
                    __func__,
                    " not implemented for '",
                    toString(g0.scalar_type()),
                    "' for the convection vector.");
            }
        });

        return std::make_tuple(out, out_grad_field);
    }
    else
    {
        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a =
                input
                    .packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto out_a =
                out.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

            switch (g0.scalar_type())
            {
            case torch::kFloat32:
            {
                auto g0_a =
                    g0.packed_accessor32<float, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_no_grad_kernel<scalar_t, float>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            case torch::kFloat64:
            {
                auto g0_a =
                    g0.packed_accessor32<double, 2, torch::RestrictPtrTraits>();

                void* args[] = {&input_a, &g0_a, &out_a};
                CUDA_CALL(cudaLaunchKernel(
                    (void*)convection_fw_no_grad_kernel<scalar_t, double>,
                    blocks(input),
                    threads(input),
                    args));
                break;
            }

            default:
                TORCH_CHECK(
                    false,
                    __func__,
                    " not implemented for '",
                    toString(g0.scalar_type()),
                    "' for the convection vector.");
            }
        });

        return std::make_tuple(out, torch::empty({0}));
    }
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cuda(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field)
{
    using namespace torch::indexing;

    const auto input_grad = torch::zeros_like(grad);
    const auto g0_splits = torch::zeros_like(
        out_grad_field, torch::TensorOptions().dtype(g0.scalar_type()));

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        auto grad_a =
            grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto input_grad_a =
            input_grad
                .packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto out_grad_field_a =
            out_grad_field
                .packed_accessor32<scalar_t, 6, torch::RestrictPtrTraits>();

        switch (g0.scalar_type())
        {

        case at::ScalarType::Float:
        {
            auto g0_a =
                g0.packed_accessor32<float, 2, torch::RestrictPtrTraits>();
            auto g0_splits_a =
                g0_splits
                    .packed_accessor32<float, 6, torch::RestrictPtrTraits>();

            void* args[] = {
                &g0_a, &grad_a, &input_grad_a, &out_grad_field_a, &g0_splits_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)convection_bw_kernel<scalar_t, float>,
                blocks(grad),
                threads(grad),
                args));

            break;
        }

        case at::ScalarType::Double:
        {
            auto g0_a =
                g0.packed_accessor32<double, 2, torch::RestrictPtrTraits>();
            auto g0_splits_a =
                g0_splits
                    .packed_accessor32<double, 6, torch::RestrictPtrTraits>();

            void* args[] = {
                &g0_a, &grad_a, &input_grad_a, &out_grad_field_a, &g0_splits_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)convection_bw_kernel<scalar_t, double>,
                blocks(grad),
                threads(grad),
                args));

            break;
        }

        default:
            TORCH_CHECK(
                false,
                __func__,
                " not implemented for '",
                toString(g0.scalar_type()),
                "' for the convection vector.");
        }
    });

    const auto g0_grad = g0_splits.sum({0, 2, 3, 4});

    return std::make_tuple(input_grad, g0_grad);
}

/*

*/
torch::Tensor
linear_convolution_fw_cuda(
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    const auto out = torch::zeros_like(input);

    const auto c = input.size(1);
    const auto ors = input.size(2);

    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    const auto stack_ors = (ker_ors % 2 == 0) ? ker_ors + 1 : ker_ors;
    const auto stack_h = (ker_h % 2 == 0) ? ker_h + 1 : ker_h;
    const auto stack_w = (ker_w % 2 == 0) ? ker_w + 1 : ker_w;

    const auto kstack = torch::zeros({ors, c, stack_ors, stack_h, stack_w}, kernel.options());

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        auto input_a = input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto kernel_a = kernel.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto out_a = out.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto kstack_a = kstack.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
       
        auto boundary = static_cast<scalar_t>(0);

        void* args1[] = {&kernel_a, &kstack_a, &boundary};

        CUDA_CALL(cudaLaunchKernel(
            (void*)rotated_kernel_stack_nearest_kernel<scalar_t>,
            blocks(kstack),
            threads(kstack),
            args1
        ));

        void* args2[] = {&input_a, &kstack_a, &out_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)linear_convolution_fw_kernel<scalar_t>,
            blocks(input),
            threads(input),
            args2
        ));
    });

    return out;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_convolution_bw_cuda(
    const torch::Tensor& grad,  // [B,C,Ors,H,W]
    const torch::Tensor& input, // [B,C,Ors,H,W]
    const torch::Tensor& kernel // [C,kOrs,kH,kW]
)
{
    const auto b = grad.size(0);
    const auto c = kernel.size(0);
    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    const auto input_grad = torch::zeros_like(grad);
    const auto kernel_grad = torch::zeros({b, c, ker_ors, ker_h, ker_w}, grad.options());

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        auto input_a = input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto kernel_a = kernel.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto grad_a = grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto input_grad_a = input_grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto kernel_grad_a = kernel_grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

        void* args[] = {&input_a, &kernel_a, &grad_a, &input_grad_a, &kernel_grad_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)linear_convolution_bw_kernel<scalar_t>,
            blocks(grad),
            threads(grad),
            args
        ));
    });
    

    return std::make_tuple(input_grad, kernel_grad.sum({0}));
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cuda(
    const torch::Tensor& input, const torch::Tensor& kernel)
{
    const auto out = torch::zeros_like(input);

    const auto c = input.size(1);
    const auto ors = input.size(2);

    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    const auto stack_ors = (ker_ors % 2 == 0) ? ker_ors + 1 : ker_ors;
    const auto stack_h = (ker_h % 2 == 0) ? ker_h + 1 : ker_h;
    const auto stack_w = (ker_w % 2 == 0) ? ker_w + 1 : ker_w;

    const auto kstack = torch::zeros({ors, c, stack_ors, stack_h, stack_w}, kernel.options());

    if (input.requires_grad() || kernel.requires_grad())
    {
        std::vector<int64_t> backindex_size =
            input.sizes().vec();     // = {B,C,Or,H,W}
        backindex_size.push_back(3); // = {B,C,Or,H,W,3}
        const auto backindex = torch::zeros(
            backindex_size,
            torch::TensorOptions().dtype(torch::kInt32).device(input.device()));

        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a = input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto kernel_a = kernel.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto out_a = out.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto backindex_a = backindex.packed_accessor32<int32_t, 6, torch::RestrictPtrTraits>();
            auto kstack_a = kstack.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

            auto boundary = std::numeric_limits<scalar_t>::max();

            void* args1[] = {&kernel_a, &kstack_a, &boundary};

            CUDA_CALL(cudaLaunchKernel(
                (void*)rotated_kernel_stack_nearest_kernel<scalar_t>,
                blocks(kstack),
                threads(kstack),
                args1
            ));

            void* args2[] = {&input_a, &kstack_a, &out_a, &backindex_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)morphological_convolution_fw_kernel<scalar_t>,
                blocks(input),
                threads(input),
                args2
            ));
        });

        return std::make_tuple(out, backindex);
    }
    else
    {
        AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
            auto input_a = input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto kernel_a = kernel.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
            auto out_a = out.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
            auto kstack_a = kstack.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

            auto boundary = std::numeric_limits<scalar_t>::max();

            void* args1[] = {&kernel_a, &kstack_a, &boundary};

            CUDA_CALL(cudaLaunchKernel(
                (void*)rotated_kernel_stack_nearest_kernel<scalar_t>,
                blocks(kstack),
                threads(kstack),
                args1
            ));

            void* args2[] = {&input_a, &kstack_a, &out_a};

            CUDA_CALL(cudaLaunchKernel(
                (void*)morphological_convolution_fw_no_grad_kernel<scalar_t>,
                blocks(input),
                threads(input),
                args2
            ));
        });

        return std::make_tuple(out, torch::empty({0}));
    }
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes)
{
    const auto B = grad.size(0);
    const auto C = kernel_sizes[0];
    const auto kOr = kernel_sizes[1];
    const auto kH = kernel_sizes[2];
    const auto kW = kernel_sizes[3];
    const auto input_grad = torch::zeros_like(grad);
    const auto kernel_grad = torch::zeros({B, C, kOr, kH, kW}, grad.options());
    const auto backindex2 = backindex.to(torch::kInt32);

    AT_DISPATCH_FLOATING_TYPES(grad.scalar_type(), __func__, [&] {
        auto grad_a = grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto backindex_a = backindex2.packed_accessor32<int32_t, 6, torch::RestrictPtrTraits>();
        auto input_grad_a = input_grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto kernel_grad_a = kernel_grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

        void* args[] = {&grad_a, &backindex_a, &input_grad_a, &kernel_grad_a};

        // CUDA_CALL(cudaLaunchKernel(
        //     (void*)morphological_convolution_bw_input_grad_kernel<scalar_t>,
        //     blocks(input_grad),
        //     threads(input_grad),
        //     args
        // ));

        CUDA_CALL(cudaLaunchKernel(
            (void*)morphological_convolution_bw_kernel<scalar_t>,
            blocks(grad),
            threads(grad),
            args
        ));
    });

    return std::make_tuple(input_grad, kernel_grad.sum({0}));
}


/*

*/
torch::Tensor
rotated_kernel_stack_nearest_cuda(
    const torch::Tensor& kernel, 
    const int64_t orientations
)
{
    const auto c = kernel.size(0);
    const auto ker_ors = kernel.size(1);
    const auto ker_h = kernel.size(2);
    const auto ker_w = kernel.size(3);

    const auto stack_ors = (ker_ors % 2 == 0) ? ker_ors + 1 : ker_ors;
    const auto stack_h = (ker_h % 2 == 0) ? ker_h + 1 : ker_h;
    const auto stack_w = (ker_w % 2 == 0) ? ker_w + 1 : ker_w;

    const auto kstack = torch::zeros({orientations, c, stack_ors, stack_h, stack_w}, kernel.options());

    AT_DISPATCH_FLOATING_TYPES(kernel.scalar_type(), __func__, [&] {
        auto kernel_a = kernel.packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();
        auto kstack_a = kstack.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

        scalar_t scalar_max = std::numeric_limits<scalar_t>::max();

        void* args[] = {&kernel_a, &kstack_a, &scalar_max};

        CUDA_CALL(cudaLaunchKernel(
            (void*)rotated_kernel_stack_nearest_kernel<scalar_t>,
            blocks(kstack),
            threads(kstack),
            args));
    });

    return kstack;
}

/*

*/
torch::Tensor
linear_fw_cuda(const torch::Tensor& input, const torch::Tensor& weight)
{
    const auto B = input.size(0);
    const auto Cin = input.size(1);
    const auto Or = input.size(2);
    const auto H = input.size(3);
    const auto W = input.size(4);
    const auto Cout = weight.size(1);

    const auto out = torch::zeros({B, Cout, Or, H, W}, input.options());

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        auto input_a =
            input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto weight_a =
            weight.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto out_a =
            out.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();

        // linear_fw_cpu_impl<scalar_t>(input_a, weight_a, out_a);
        void* args[] = {&input_a, &weight_a, &out_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)linear_fw_kernel<scalar_t>,
            blocks(out),
            threads(out),
            args));
    });

    return out;
}

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& weight)
{
    const auto B = input.size(0);
    const auto Cin = input.size(1);
    const auto Or = input.size(2);
    const auto H = input.size(3);
    const auto W = input.size(4);
    const auto Cout = weight.size(1);

    const auto input_grad = torch::zeros_like(input);
    const auto weight_grad = torch::zeros({B, Or, Cin, Cout}, weight.options());

    AT_DISPATCH_FLOATING_TYPES(input.scalar_type(), __func__, [&] {
        auto grad_a =
            grad.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto input_a =
            input.packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto weight_a =
            weight.packed_accessor32<scalar_t, 2, torch::RestrictPtrTraits>();
        auto input_grad_a =
            input_grad
                .packed_accessor32<scalar_t, 5, torch::RestrictPtrTraits>();
        auto weight_grad_a =
            weight_grad
                .packed_accessor32<scalar_t, 4, torch::RestrictPtrTraits>();

        // linear_bw_cpu_impl<scalar_t>(
        //    grad_a, input_a, weight_a, input_grad_a, weight_grad_a);
        void* args[] = {
            &grad_a, &input_a, &weight_a, &input_grad_a, &weight_grad_a};

        CUDA_CALL(cudaLaunchKernel(
            (void*)linear_bw_kernel<scalar_t>,
            blocks(input),
            threads(input),
            args));
    });

    return std::make_tuple(
        input_grad, weight_grad.sum(torch::IntArrayRef({0, 1})));
}

} // namespace m2
} // namespace lietorch