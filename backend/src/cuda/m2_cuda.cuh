#pragma once
#ifndef LIETORCH_CUDA_M2_CUDA_CUH
#define LIETORCH_CUDA_M2_CUDA_CUH

/** @file m2_cuda.h CUDA implementation of M2 operations.
 *
 */

#include "../torch_include.h"

namespace lietorch
{
namespace m2
{

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
anisotropic_dilated_project_fw_cuda(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha,
    const double scale);

/*

*/
torch::Tensor
anisotropic_dilated_project_bw_cuda(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const torch::List<int64_t> input_shape);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cuda(const torch::Tensor& input, const torch::Tensor& g0);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cuda(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field);

/*

*/
torch::Tensor
linear_convolution_fw_cuda(
    const torch::Tensor& input, const torch::Tensor& kernel);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_convolution_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& kernel);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cuda(
    const torch::Tensor& input, const torch::Tensor& kernel);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& back_index,
    const torch::IntArrayRef kernel_sizes);

/*

*/
torch::Tensor
rotated_kernel_stack_nearest_cuda(
    const torch::Tensor& kernel, 
    const int64_t orientations
);

/*

*/
torch::Tensor
linear_fw_cuda(const torch::Tensor& input, const torch::Tensor& weight);

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
linear_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& weight);

} // namespace m2
} // namespace lietorch

#endif