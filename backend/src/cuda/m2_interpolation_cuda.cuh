#pragma once
#ifndef LIETORCH_CUDA_M2_INTERPOLATION_CUDA_CUH
#define LIETORCH_CUDA_M2_INTERPOLATION_CUDA_CUH
/**
 * @file cuda/m2_interpolation_cuda.cuh
 *
 * @brief
 */

#include "../torch_include.h"

#include "../m2_element.h"
#include "atomics.cuh"
#include "dispatch.cuh"
#include "math_cuda.cuh"
#define _USE_MATH_DEFINES
#include <cmath>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_constants.h>

namespace lietorch
{
namespace m2
{

/**
 *  @brief Sample a point using trilinear interpolation, specified in array
 * coordinates sample_i, sample_j and sample_k, on the M2 manifold by
 * interpolating the 3D tensor input_a, additionally returns the gradient in the
 * Cartesian frame at the sampled point.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param sample_i orientation coordinate
 *  @param sample_j y coordinate
 *  @param sample_k x coordinate
 *
 *  @return {value, dz, dy, dx}
 */
template <typename scalar_t, typename coordinate_t>
__device__ __forceinline__ std::tuple<scalar_t, scalar_t, scalar_t, scalar_t>
interpolate_m2_trilinear_with_grad_cuda(
    const at::TensorAccessor<scalar_t, 3ULL, at::RestrictPtrTraits, int>&
        input_a,
    const coordinate_t sample_i,
    const coordinate_t sample_j,
    const coordinate_t sample_k)
{
    const int32_t ors = input_a.size(0);
    const int32_t h = input_a.size(1);
    const int32_t w = input_a.size(2);

    if (sample_j <= -1 || sample_j >= h || sample_k <= -1 || sample_k >= w)
    {
        const scalar_t zero = static_cast<scalar_t>(0);
        return {zero, zero, zero, zero};
    }

    const auto base_offset_i = lt_cu_intfrac<coordinate_t>(sample_i);
    const int32_t base_i = static_cast<int32_t>(std::get<0>(base_offset_i));
    const int32_t base_i0 = base_i >= 0 ? base_i : base_i + ors;
    const int32_t base_i1 = (base_i0 + 1) % ors;
    const scalar_t offset_i = static_cast<scalar_t>(std::get<1>(base_offset_i));

    const auto base_offset_j = lt_cu_intfrac<coordinate_t>(sample_j);
    const int32_t base_j = static_cast<int32_t>(std::get<0>(base_offset_j));
    const scalar_t offset_j = static_cast<scalar_t>(std::get<1>(base_offset_j));

    const auto base_offset_k = lt_cu_intfrac<coordinate_t>(sample_k);
    const int32_t base_k = static_cast<int32_t>(std::get<0>(base_offset_k));
    const scalar_t offset_k = static_cast<scalar_t>(std::get<1>(base_offset_k));

    const scalar_t v000 =
        (base_j < 0 || base_k < 0) ? 0 : input_a[base_i0][base_j][base_k];
    const scalar_t v100 =
        (base_j < 0 || base_k < 0) ? 0 : input_a[base_i1][base_j][base_k];
    const scalar_t v010 = (base_j >= h - 1 || base_k < 0)
                              ? 0
                              : input_a[base_i0][base_j + 1][base_k];
    const scalar_t v110 = (base_j >= h - 1 || base_k < 0)
                              ? 0
                              : input_a[base_i1][base_j + 1][base_k];
    const scalar_t v001 = (base_j < 0 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i0][base_j][base_k + 1];
    const scalar_t v101 = (base_j < 0 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i1][base_j][base_k + 1];
    const scalar_t v011 = (base_j >= h - 1 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i0][base_j + 1][base_k + 1];
    const scalar_t v111 = (base_j >= h - 1 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i1][base_j + 1][base_k + 1];

    const scalar_t v00 = (1 - offset_i) * v000 + offset_i * v100;
    const scalar_t v10 = (1 - offset_i) * v010 + offset_i * v110;
    const scalar_t v01 = (1 - offset_i) * v001 + offset_i * v101;
    const scalar_t v11 = (1 - offset_i) * v011 + offset_i * v111;

    const scalar_t v0 = (1 - offset_j) * v00 + offset_j * v10;
    const scalar_t v1 = (1 - offset_j) * v01 + offset_j * v11;

    const scalar_t v = (1 - offset_k) * v0 + offset_k * v1;

    const scalar_t dx = v1 - v0;
    const scalar_t dy = (1 - offset_k) * (v10 - v00) + offset_k * (v11 - v01);

    const scalar_t v0dz =
        (1 - offset_j) * (v100 - v000) + offset_j * (v110 - v010);
    const scalar_t v1dz =
        (1 - offset_j) * (v101 - v001) + offset_j * (v111 - v011);
    const scalar_t dz = (1 - offset_k) * v0dz + offset_k * v1dz;

    return std::make_tuple(v, dz, dy, dx);
}

/**
 *  @brief Sample a point using trilinear interpolation, specified by a
 * m2::element, on the M2 manifold by interpolating the 3D tensor input_a,
 * additionally returns the gradient in the Cartesian frame at the sampled
 * point.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param g Group element that specifies where to sample.
 *
 *  @return {value, dz, dy, dx}
 */
template <typename scalar_t, typename coordinate_t>
__device__ __forceinline__ std::tuple<scalar_t, scalar_t, scalar_t, scalar_t>
interpolate_m2_trilinear_with_grad_cuda(
    const at::TensorAccessor<scalar_t, 3ULL, at::RestrictPtrTraits, int>&
        input_a,
    const m2::element<coordinate_t>& g)
{
    const coordinate_t ors = static_cast<coordinate_t>(input_a.size(0));
    const coordinate_t or_co = ors * g.t / g.t_scale;
    return interpolate_m2_trilinear_with_grad_cuda(input_a, or_co, g.y, g.x);
}

/**
 *  @brief Sample a point using trilinear interpolation, specified in array
 * coordinates sample_i, sample_j and sample_k, on the M2 manifold by
 * interpolating the 3D tensor input_a.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param sample_i orientation coordinate
 *  @param sample_j y coordinate
 *  @param sample_k x coordinate
 *
 *  @return value
 */
template <typename scalar_t, typename coordinate_t>
__device__ __forceinline__ scalar_t
interpolate_m2_trilinear_cuda(
    const at::TensorAccessor<scalar_t, 3ULL, at::RestrictPtrTraits, int>&
        input_a,
    const coordinate_t sample_i,
    const coordinate_t sample_j,
    const coordinate_t sample_k)
{
    const int32_t ors = input_a.size(0);
    const int32_t h = input_a.size(1);
    const int32_t w = input_a.size(2);

    if (sample_j <= -1 || sample_j >= h || sample_k <= -1 || sample_k >= w)
    {
        return static_cast<scalar_t>(0);
    }

    const auto base_offset_i = lt_cu_intfrac<coordinate_t>(sample_i);
    const int32_t base_i = static_cast<int32_t>(std::get<0>(base_offset_i));
    const int32_t base_i0 = base_i >= 0 ? base_i : base_i + ors;
    const int32_t base_i1 = (base_i0 + 1) % ors;
    const scalar_t offset_i = static_cast<scalar_t>(std::get<1>(base_offset_i));

    const auto base_offset_j = lt_cu_intfrac<coordinate_t>(sample_j);
    const int32_t base_j = static_cast<int32_t>(std::get<0>(base_offset_j));
    const scalar_t offset_j = static_cast<scalar_t>(std::get<1>(base_offset_j));

    const auto base_offset_k = lt_cu_intfrac<coordinate_t>(sample_k);
    const int32_t base_k = static_cast<int32_t>(std::get<0>(base_offset_k));
    const scalar_t offset_k = static_cast<scalar_t>(std::get<1>(base_offset_k));

    const scalar_t v000 =
        (base_j < 0 || base_k < 0) ? 0 : input_a[base_i0][base_j][base_k];
    const scalar_t v100 =
        (base_j < 0 || base_k < 0) ? 0 : input_a[base_i1][base_j][base_k];
    const scalar_t v010 = (base_j >= h - 1 || base_k < 0)
                              ? 0
                              : input_a[base_i0][base_j + 1][base_k];
    const scalar_t v110 = (base_j >= h - 1 || base_k < 0)
                              ? 0
                              : input_a[base_i1][base_j + 1][base_k];
    const scalar_t v001 = (base_j < 0 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i0][base_j][base_k + 1];
    const scalar_t v101 = (base_j < 0 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i1][base_j][base_k + 1];
    const scalar_t v011 = (base_j >= h - 1 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i0][base_j + 1][base_k + 1];
    const scalar_t v111 = (base_j >= h - 1 || base_k >= w - 1)
                              ? 0
                              : input_a[base_i1][base_j + 1][base_k + 1];

    const scalar_t v00 = (1 - offset_i) * v000 + offset_i * v100;
    const scalar_t v10 = (1 - offset_i) * v010 + offset_i * v110;
    const scalar_t v01 = (1 - offset_i) * v001 + offset_i * v101;
    const scalar_t v11 = (1 - offset_i) * v011 + offset_i * v111;

    const scalar_t v0 = (1 - offset_j) * v00 + offset_j * v10;
    const scalar_t v1 = (1 - offset_j) * v01 + offset_j * v11;

    const scalar_t v = (1 - offset_k) * v0 + offset_k * v1;

    return v;
}

/**
 *  @brief Sample a point using trilinear interpolation, specified by a
 * m2::element, on the M2 manifold by interpolating the 3D tensor input_a.
 *
 *  @tparam scalar_t Type of the scalar data
 *  @tparam coordinate_t Type of the coordinates
 *
 *  @param input_a A 3 dimensional tensor(-accessor) that will be interpolated
 *  @param g Group element that specifies where to sample.
 *
 *  @return value
 */
template <typename scalar_t, typename coordinate_t>
__device__ __forceinline__ scalar_t
interpolate_m2_trilinear_cuda(
    const at::TensorAccessor<scalar_t, 3ULL, at::RestrictPtrTraits, int>&
        input_a,
    const m2::element<coordinate_t>& g)
{
    const coordinate_t ors = static_cast<coordinate_t>(input_a.size(0));
    const coordinate_t or_co = ors * g.t / g.t_scale;
    return interpolate_m2_trilinear_cuda(input_a, or_co, g.y, g.x);
}

} // namespace m2
} // namespace lietorch

#endif