#pragma once
#ifndef LIETORCH_CUDA_MATH_CUDA_CUH
#define LIETORCH_CUDA_MATH_CUDA_CUH

/** @file math_cuda.cuh
 *  @brief Helper math operations for CUDA devices.
 *
 */

#define _USE_MATH_DEFINES
#include <cmath>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <math_constants.h>
#include <tuple>

namespace lietorch
{

// Pi constant for different types
template <typename T>
__forceinline__ __device__ constexpr T
LT_CU_PI()
{
    return T(CUDART_PI);
}

template <>
__forceinline__ __device__ constexpr float
LT_CU_PI<float>()
{
    return CUDART_PI_F;
}

/**
 * @brief Efficient simultaneous calculation of cosine and sine on GPU.
 * This version calculates the cos and sin of PI*x not of x!
 * Prefer this version over lt_cu_cossin if possible.
 */
template <typename T>
__forceinline__ __device__ std::tuple<T, T>
lt_cu_cossinpi(T x)
{
    return std::make_tuple(cos(x * LT_CU_PI<T>), sin(x * LT_CU_PI<T>));
}

template <>
__forceinline__ __device__ std::tuple<float, float>
lt_cu_cossinpi<float>(float x)
{
    float c, s;
    sincospif(x, &s, &c);
    return std::make_tuple(c, s);
}

template <>
__forceinline__ __device__ std::tuple<double, double>
lt_cu_cossinpi<double>(double x)
{
    double c, s;
    sincospi(x, &s, &c);
    return std::make_tuple(c, s);
}

/**
 * @brief Efficient simultaneous calculation of cosine and sine on GPU.
 * This version calculates the cos and sin of x directly.
 *
 */
template <typename T>
__forceinline__ __device__ std::tuple<T, T>
lt_cu_cossin(T x)
{
    return std::make_tuple(cos(x), sin(x));
}

template <>
__forceinline__ __device__ std::tuple<float, float>
lt_cu_cossin<float>(float x)
{
    float c, s;
    sincosf(x, &s, &c);
    return std::make_tuple(c, s);
}

template <>
__forceinline__ __device__ std::tuple<double, double>
lt_cu_cossin<double>(double x)
{
    double c, s;
    sincos(x, &s, &c);
    return std::make_tuple(c, s);
}

/**
 *  @brief Floating point remainder
 *
 */
template <typename T>
__forceinline__ __device__ T
lt_cu_fmod(T x, T y)
{
    return fmod(x, y);
}

template <>
__forceinline__ __device__ float
lt_cu_fmod<float>(float x, float y)
{
    return fmodf(x, y);
}

template <>
__forceinline__ __device__ double
lt_cu_fmod<double>(double x, double y)
{
    return fmod(x, y);
}

/**
 *
 * @brief Breaks down the input into fractional and integral parts so that the
 * fractional part is always non-negative.
 *
 * @example
 *      lt_cu_intfrac(-1.1) returns {-2.0, 0.9}
 */
template <typename T>
__forceinline__ __device__ std::tuple<T, T>
lt_cu_intfrac(T x)
{
    T int_part;
    T frac_part = std::modf(x, &int_part);
    if (frac_part < 0)
    {
        frac_part += 1;
        int_part -= 1;
    }
    return std::make_tuple(int_part, frac_part);
}

template <>
__forceinline__ __device__ std::tuple<float, float>
lt_cu_intfrac<float>(float x)
{
    float int_part;
    float frac_part = modff(x, &int_part);
    if (frac_part < 0)
    {
        frac_part += 1;
        int_part -= 1;
    }
    return std::make_tuple(int_part, frac_part);
}

template <>
__forceinline__ __device__ std::tuple<double, double>
lt_cu_intfrac<double>(double x)
{
    double int_part;
    double frac_part = modf(x, &int_part);
    if (frac_part < 0)
    {
        frac_part += 1;
        int_part -= 1;
    }
    return std::make_tuple(int_part, frac_part);
}

} // namespace lietorch

#endif