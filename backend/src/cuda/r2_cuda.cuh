#pragma once
#ifndef LIETORCH_CUDA_R2_CUDA_H
#define LIETORCH_CUDA_R2_CUDA_H

#include "../torch_include.h"

namespace lietorch
{
namespace r2
{

/*

*/
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw_cuda(
    const torch::Tensor& input, const torch::Tensor& kernel);

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw_cuda(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes);

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw_cuda(const torch::Tensor& input, const torch::Tensor& g0);

/*

 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw_cuda(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field);

} // namespace r2
} // namespace lietorch

#endif