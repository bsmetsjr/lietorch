#pragma once
#ifndef LIETORCH_GENERIC_H
#define LIETORCH_GENERIC_H

/** @file generic.h
 *  @brief Generic functions mainly used as examples of how to implement new
 * ops.
 */

#include "torch_include.h"

namespace lietorch
{
namespace generic
{

/**
 *  @brief Add two tensors of the same shape and type.
 */
torch::Tensor
add_fw(const torch::Tensor& a, const torch::Tensor& b);

/**
 *  @brief Backpropagation of add_fw.
 */
std::tuple<torch::Tensor, torch::Tensor>
add_bw(const torch::Tensor& grad);

/**
 *  @brief Dilation of 2D grayscale image.
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_fw(
    const torch::Tensor& image, const torch::Tensor& filter);

/**
 *  @brief Backpropagation of lietorch::generic::grayscale_dilation_2d_fw .
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_dilation_2d_bw(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w);

/**
 *  @brief Erosion of 2D grayscale image.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_fw(
    const torch::Tensor& image, const torch::Tensor& filter);

/**
 *  @brief Backpropagation of lietorch::generic::grayscale_erosion_2d_fw .
 */
std::tuple<torch::Tensor, torch::Tensor>
grayscale_erosion_2d_bw(
    const torch::Tensor& backindex,
    const torch::Tensor& grad,
    const int64_t filter_h,
    const int64_t filter_w);

using torch::autograd::AutogradContext;
using torch::autograd::Function;
using torch::autograd::Variable;
using torch::autograd::variable_list;

/**
 *
 *
 *
 */
class GrayscaleDilation2D : public Function<GrayscaleDilation2D>
{
public:
    static variable_list
    forward(AutogradContext* ctx, Variable image, Variable filter);

    static variable_list backward(AutogradContext* ctx, variable_list grads);
};

torch::Tensor
grayscale_dilation_2d(const torch::Tensor& image, const torch::Tensor& filter);

/**
 *
 *
 *
 */
class GrayscaleErosion2D : public Function<GrayscaleErosion2D>
{
public:
    static variable_list
    forward(AutogradContext* ctx, Variable image, Variable filter);

    static variable_list backward(AutogradContext* ctx, variable_list grads);
};

torch::Tensor
grayscale_erosion_2d(const torch::Tensor& image, const torch::Tensor& filter);

} // namespace generic
} // namespace lietorch

#endif