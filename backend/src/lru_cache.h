#pragma once
#ifndef LIETORCH_LRU_CACHE_H
#define LIETORCH_LRU_CACHE_H

#include <cstddef>
#include <functional>
#include <iostream>
#include <mutex>
#include <optional>
#include <stdexcept>
#include <tuple>
#include <unordered_map>


//////////////////////////////////////////////////
//
//  Hashing tuples
//
//////////////////////////////////////////////////

namespace std {


template <typename T>
constexpr size_t
hash_combine(const T& v, size_t seed) noexcept
{
    return seed ^ (std::hash<T>()(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2));
}

// Hashing tuples
template <typename... Args> 
struct hash<std::tuple<Args...>>
{
    size_t operator()(const std::tuple<Args...>& tuple) const noexcept
    {
        return std::apply(
            [](const auto&... args) {
                auto seed = static_cast<std::size_t>(672807365);
                ((seed = std::hash_combine(args, seed)), ...);
                return seed;
            },
            tuple);
    }
};


// Hashing types that are already supported by the standard library
// template <typename T> struct hash
// {
//     size_t operator()(const T& val) const noexcept
//     {
//         return std::hash<T>()(val);
//     }
// };

}

//////////////////////////////////////////////////
//
//  Page/DoubleLinkedList/LRUCache definitions
//
//////////////////////////////////////////////////


namespace lietorch
{
namespace caching
{

template <typename K, typename V> class Page
{
public:
    K key;
    V value;
    Page *prev, *next;
    Page(K k, V v) : key(k), value(v), prev(nullptr), next(nullptr) {}
};

template <typename K, typename V> class DoubleLinkedList
{
private:
    Page<K, V>*head, *tail;
    bool isEmpty() const;

public:
    DoubleLinkedList() : head(nullptr), tail(nullptr) {}
    DoubleLinkedList(const DoubleLinkedList&) = delete;
    ~DoubleLinkedList();
    Page<K, V>* add_page_to_head(K key, V value);
    void move_page_to_head(Page<K, V>* page);
    void remove_tail();
    Page<K, V>* get_head() const;
    Page<K, V>* get_tail() const;

    friend std::ostream&
    operator<<(std::ostream& os, const DoubleLinkedList<K, V>& list)
    {
        if (list.isEmpty())
        {
            os << "()";
            return os;
        }

        os << "(";
        Page<K, V>* page = list.head;

        while (true)
        {
            os << page->value;
            if (page->next == nullptr)
            {
                break;
            }
            os << ", ";
            page = page->next;
        }

        os << ")";
        return os;
    }
};

template <typename K, typename V> class LRUCache
{
private:
    size_t _size, _occupancy;
    std::mutex cache_mutex;
    DoubleLinkedList<K, V> list;
    std::unordered_map<K, Page<K, V>*, std::hash<K>> map;

public:
    size_t hits{0};
    size_t misses{0};
    explicit LRUCache(size_t size = 100);
    bool contains(const K& key) const;
    std::optional<V> lookup(const K& key);
    void store(const K& key, const V& value);
    size_t size() const noexcept { return _size; };
    size_t occupancy() const noexcept { return _occupancy; };
};

//////////////////////////////////////////////////
//
//  DoubleLinkedList implementation
//
//////////////////////////////////////////////////

template <typename K, typename V> DoubleLinkedList<K, V>::~DoubleLinkedList()
{
    auto page = head;
    while (page != nullptr)
    {
        auto next = page->next;
        delete page;
        page = next;
    }
}

template <typename K, typename V>
bool
DoubleLinkedList<K, V>::isEmpty() const
{
    return tail == nullptr;
}

template <typename K, typename V>
Page<K, V>*
DoubleLinkedList<K, V>::add_page_to_head(K key, V value)
{
    Page<K, V>* page = new Page<K, V>(key, value);
    if (head == nullptr && tail == nullptr)
    {
        head = tail = page;
    }
    else
    {
        page->next = head;
        head->prev = page;
        head = page;
    }
    return page;
}

template <typename K, typename V>
void
DoubleLinkedList<K, V>::move_page_to_head(Page<K, V>* page)
{
    if (page == head)
    {
        return;
    }
    if (page == tail)
    {
        tail = tail->prev;
        tail->next = nullptr;
    }
    else
    {
        page->prev->next = page->next;
        page->next->prev = page->prev;
    }

    page->next = head;
    page->prev = nullptr;

    head->prev = page;

    head = page;
}

template <typename K, typename V>
void
DoubleLinkedList<K, V>::remove_tail()
{
    if (isEmpty())
    {
        return;
    }
    if (head == tail)
    {
        delete tail;
        head = tail = nullptr;
    }
    else
    {
        Page<K, V>* temp = tail;
        tail = tail->prev;
        tail->next = nullptr;
        delete temp;
    }
}

template <typename K, typename V>
Page<K, V>*
DoubleLinkedList<K, V>::get_head() const
{
    return head;
}

template <typename K, typename V>
Page<K, V>*
DoubleLinkedList<K, V>::get_tail() const
{
    return tail;
}

//////////////////////////////////////////////////
//
//  LRUCache implementation
//
//////////////////////////////////////////////////

template <typename K, typename V> LRUCache<K, V>::LRUCache(size_t size)
{
    _size = size;
    _occupancy = 0;
    list = DoubleLinkedList<K, V>();
    map = std::unordered_map<K, Page<K, V>*, std::hash<K>>();
}

template <typename K, typename V>
bool
LRUCache<K, V>::contains(const K& key) const
{
    return map.find(key) != map.end();
}

template <typename K, typename V>
std::optional<V>
LRUCache<K, V>::lookup(const K& key)
{
    std::lock_guard<std::mutex> guard(cache_mutex);
    try
    {
        auto page = map.at(key);
        hits++;
        list.move_page_to_head(page);
        return page->value;
    }
    catch (const std::out_of_range&)
    {
        misses++;
        return {};
    }
}

template <typename K, typename V>
void
LRUCache<K, V>::store(const K& key, const V& value)
{
    std::lock_guard<std::mutex> guard(cache_mutex);

    if (contains(key))
    {
        // if key already present, update value and move page to head
        auto page = map[key];
        page->value = value;
        list.move_page_to_head(page);
        return;
    }
    if (_occupancy == _size)
    {
        // remove last page
        const K k = list.get_tail()->key;
        map.erase(k);
        list.remove_tail();
        _occupancy--;
    }

    Page<K, V>* page = list.add_page_to_head(key, value);
    _occupancy++;
    map[key] = page;
}

} // namespace caching
} // namespace lietorch

#endif