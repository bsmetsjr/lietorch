#pragma once
#ifndef LIETORCH_M2_H
#define LIETORCH_M2_H

/** @file m2.h
 *  @brief M2 operators
 *
 */

#include "torch_include.h"

namespace lietorch
{
namespace m2
{

using torch::autograd::AutogradContext;
using torch::autograd::Function;
using torch::autograd::tensor_list;
using torch::autograd::Variable;
using torch::autograd::variable_list;

////////////////////////////////////////////////
/////
/////   Anisotropic Dilated Projection
/////
////////////////////////////////////////////////

/**
 *  @brief Forward anisotropic dilatied projection from M2 to R2
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
anisotropic_dilated_project_fw(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha = 2.0 / 3.0,
    const double scale = 1.0);

/**
 *  @brief Backpropagation of lietorch::m2::anisotorpic_dilated_projection_fw
 *
 *
 */
torch::Tensor
anisotropic_dilated_project_bw(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::List<int64_t> input_shape);

/**
 * @brief Anisotropic dilated projection per [MSc. thesis Smets].
 *
 * @param input Input M2 tensor of shape `[B,C,Or,H,W]`.
 * @param longitudinal Longitudinal extent of the elliptic structuring element
 * @param lateral Lateral extent of the elliptic structuring element
 * @param alpha Steepness of the structuring element, the closer alpha is to 1/2
 * the flatter the structuring element will be.
 * @param scale Scaling factor by which the structuring element is multiplied.
 *
 * @return A tensor of shape `[B,C,H,W]`
 */
torch::Tensor
anisotropic_dilated_project(
    const torch::Tensor& input,
    const double longitudinal,
    const double lateral,
    const double alpha = 2.0 / 3.0,
    const double scale = 1.0);

/**
 *
 */
class AnisotropicDilatedProject : public Function<AnisotropicDilatedProject>
{
public:
    static variable_list forward(
        AutogradContext* ctx,
        Variable input,
        const double longitudinal,
        const double lateral,
        const double alpha = 2.0 / 3.0,
        const double scale = 1.0);

    static variable_list backward(AutogradContext* ctx, variable_list grads);
};

////////////////////////////////////////////////
/////
/////   Convection
/////
////////////////////////////////////////////////

/**
 *  @brief Left invariant convection on M2 (forward).
 *
 *  @param input Input M2 tensor of shape `[B,C,Or,H,W]`
 *  @param g0 Convection tensor of shape `[C,3]` which consists of a triple of
 * coordinates `{theta0, y0, x0}` for each channel that specifies the convection
 * to apply.
 *
 *  @return A tuple of tensors, the first tensor is the transformed input and as
 * such has the same shape. The second tensor contains the gradient at each
 * gridpoint of the output specified in left the left invariant frame which is
 * required for a subsequent backward pass, it has shape `[B,C,Or,H,W,3]`.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
convection_fw(const torch::Tensor& input, const torch::Tensor& g0);

/**
 *  @brief Left invariant convection on M2 (backward).
 *
 *  @param g0 Convection tensor of shape `[C,3]` which consists of a triple of
 * coordinates `{theta0, y0, x0}` for each channel that specifies the convection
 * to apply.
 *  @param grad Gradient of the output (with shape `[B,C,Or,H,W]`).
 *  @param out_grad_field Second return tensor of shape `[B,C,Or,H,W,3]` of the forward pass that is
 * needed to ompute the backward pass.
 *
 * @return A tuple of tensors: the first gives the gradient of the input (with
 * shape `[B,C,Or,H,W]`), the second gives the gradient of the convection
 * parameters `g0` with shape `[C,3]`.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
convection_bw(
    const torch::Tensor& g0,
    const torch::Tensor& grad,
    const torch::Tensor& out_grad_field);

/**
 * @brief Left invariant convection on M2.
 *
 *  Note that `input` and `g0` need not have the same dtype, the output tensor
 * will have the same dtype as the input.
 *
 * @param input Tensor of shape `[B,C,Or,H,W]`
 * @param g0 Convection tensor of shape `[C,3]` which consists of a triple of
 * coordinates `{theta0, y0, x0}` for each channel that specifies the convection
 * to apply.
 *
 * @return A tensor with the same dtype and shape as the input (`[B,C,Or,H,W]`).
 */
torch::Tensor
convection(const torch::Tensor& input, const torch::Tensor& g0);

/**
 *
 * @brief Left invariant convection on M2 (Function).
 *
 */
class Convection : public Function<Convection>
{
public:
    static tensor_list
    forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor g0);

    static tensor_list backward(AutogradContext* ctx, tensor_list grads);
};

//////////////////////////////////////////////
///
/// Linear convolution
///
/////////////////////////////////////////////

/**
 * @brief Linear Convolution on M2 (Forward)
 *
 * @param input Tensor of shape `[B,C,Or,H,W]`
 * @param kernel Tensor of shape `[C,kOr,kH,kW]`
 *
 * @return Tensor of shape `[B,C,Or,H,W]`
 */
torch::Tensor
linear_convolution_fw(
    const torch::Tensor& input, const torch::Tensor& kernel);

/**
 * @brief Linear Convolution on M2 (Backward)
 *
 * @param grad Tensor of shape `[B,C,Ors,W,H]`
 * @param input Tensor of shape `[B,C,Ors,W,H]'
 * @param kernel Tensor of shape `[C, kOr, kH, kW]`
 *
 * @return A pair of tensors: the first is the gradient with respect to the
 * input, the second the gradient with respect to the kernel.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
linear_convolution_bw(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& kernel);

/**
 * @brief linear convolution on M2.
 *
 * @param input Tensor of shape `[B,C,Or,H,W]`
 * @param kernel Tensor of shape `[C,kOr,kH,kW]`
 *
 * @return A tensor with the same dtype and shape as the input (`[B,C,Or,H,W]`).
 */
torch::Tensor
linear_convolution(
    const torch::Tensor& input, const torch::Tensor& kernel);

/**
 *
 * @brief linear convolution on M2 (Function).
 *
 */
class LinearConvolution : public Function<LinearConvolution>
{
public:
    static tensor_list
    forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor kernel);

    static tensor_list backward(AutogradContext* ctx, tensor_list grads);
};

//////////////////////////////////////////////
///
/// Morphological convolution
///
/////////////////////////////////////////////

/**
 * @brief Morphological Convolution on M2 (Forward)
 *
 * @param input Tensor of shape `[B,C,Or,H,W]`
 * @param kernel Kernel tensor of shape `[C,kOr,kH,kW]` of the same scalar type
 * as input.
 *
 * @return A pair of tensors: the first the output of shape `[B,C,Or,H,W]` and a
 * back_index tensor of shape `[B,C,Or,H,W,3]` that is needed for the backward
 * pass.
 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_fw(
    const torch::Tensor& input, const torch::Tensor& kernel);

/**
 * @brief Morphological Convolution on M2 (Backward)
 *
 * @param grad Gradient of the output (with shape `[B,C,Ors,W,H]`)
 * @param back_index Back index tensor of shape `[B,C,Or,H,W,3]` from the forward pass
 * @param kernel_sizes Size of the original kernel: `{C, kOr, kH, kW}`
 *
 * @return A pair of tensors: the first is the gradient with respect to the
 * input, the second the gradient with respect to the kernel.
 *
 */
std::tuple<torch::Tensor, torch::Tensor>
morphological_convolution_bw(
    const torch::Tensor& grad,
    const torch::Tensor& backindex,
    const torch::IntArrayRef kernel_sizes);

/**
 * @brief Morphological convolution on M2.
 *
 *
 * @param input Tensor of shape `[B,C,Or,H,W]`
 * @param kernel Kernel tensor of shape `[C,kOr,kH,kW]`
 *
 * @return A tensor with the same dtype and shape as the input (`[B,C,Or,H,W]`).
 */
torch::Tensor
morphological_convolution(
    const torch::Tensor& input, const torch::Tensor& kernel);

/**
 *
 * @brief Morphological convolution on M2 (Function).
 *
 */
class MorphologicalConvolution : public Function<MorphologicalConvolution>
{
public:
    static tensor_list
    forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor kernel);

    static tensor_list backward(AutogradContext* ctx, tensor_list grads);
};

/**
 *
 * @brief Squared logarithmic metric estimate on M2 for a left invariant
 * Riemannian metric tensor field.
 *
 * @param metric_params Tensor of shape `[..., 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 *
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 *
 */
torch::Tensor
logarithmic_metric_estimate_squared(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale);

/**
 *
 * @brief Squared logarithmic metric estimate on M2 for a non-diagonal
 * left invariant Riemannian metric tensor field.
 *
 * @param metric_params Tensor of shape `[..., 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 *
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 *
 */
torch::Tensor
logarithmic_metric_estimate_squared_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale);

/**
 *
 * @brief Logarithmic metric estimate on M2 for a left invariant
 * Riemannian metric tensor field.
 *
 * @param metric_params Tensor of shape `[..., 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 * @param t_scale How much the orientation index needs to increase to consitute
 * a full rotation, i.e. t_index * 2π / t_scale = θ.
 *
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 *
 */
torch::Tensor
logarithmic_metric_estimate(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale);

/**
 *
 * @brief Logarithmic metric estimate on M2 for a non-diagonal left invariant
 * Riemannian metric tensor field.
 *
 * @param metric_params Tensor of shape `[..., 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 * @param t_scale How much the orientation index needs to increase to consitute
 * a full rotation, i.e. t_index * 2π / t_scale = θ.
 *
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 *
 */
torch::Tensor
logarithmic_metric_estimate_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale);

/**
 * @brief Approximation of the dilation/erosion kernel.
 *
 * @param metric_params Tensor of shape `[..., 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 * @param t_scale How much the orientation index needs to increase to consitute
 * a full rotation, i.e. t_index * 2π / t_scale = θ.
 * @param alpha Alpha parameter, needs to be in range [0.55, 1.0].
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 */
torch::Tensor
morphological_kernel(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale,
    const double alpha);

/**
 * @brief Approximation of the dilation/erosion kernel.
 *
 * @param metric_params Tensor of shape `[..., 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 * @param t_scale How much the orientation index needs to increase to consitute
 * a full rotation, i.e. t_index * 2π / t_scale = θ.
 * @param alpha Alpha parameter, needs to be in range [0.55, 1.0].
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 */
torch::Tensor
morphological_kernel_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale,
    const double alpha);

/**
 * @brief Approximation of the diffusion kernel.
 *
 * @param metric_params Tensor of shape `[..., 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 * @param t_scale How much the orientation index needs to increase to consitute
 * a full rotation, i.e. t_index * 2π / t_scale = θ.
 * @param alpha Alpha parameter, needs to be in range [0.55, 1.0].
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 */
torch::Tensor
diffusion_kernel(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale);

/**
 * @brief Approximation of the diffusion kernel.
 *
 * @param metric_params Tensor of shape `[..., 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr, kH,
 * kW}`
 * @param t_scale How much the orientation index needs to increase to consitute
 * a full rotation, i.e. t_index * 2π / t_scale = θ.
 * @param alpha Alpha parameter, needs to be in range [0.55, 1.0].
 *
 * @return Tensor of shape `[..., kOrs, kH, kW]`
 */
torch::Tensor
diffusion_kernel_nondiag(
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double t_scale);

/**
 * @brief M2 left invariant dilation
 *
 * @param input Tensor of shape `[B, C, Or, H, W]`
 * @param metric_params Tensor of shape `[C, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr,kH,kW}`
 * @param alpha Alpha parameter, needs to be in the range [0.55, 1.0].
 *
 * @return Tensor of shape `[B, C, Or, H, W]`
 */
torch::Tensor
fractional_dilation(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha);

/**
 * @brief M2 left invariant dilation
 *
 * @param input Tensor of shape `[B, C, Or, H, W]`
 * @param metric_params Tensor of shape `[C, 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr,kH,kW}`
 * @param alpha Alpha parameter, needs to be in the range [0.55, 1.0].
 *
 * @return Tensor of shape `[B, C, Or, H, W]`
 */
torch::Tensor
fractional_dilation_nondiag(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha);

/**
 * @brief M2 left invariant erosion
 *
 * @param input Tensor of shape `[B, C, Or, H, W]`
 * @param metric_params Tensor of shape `[C, 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr,kH,kW}`
 * @param alpha Alpha parameter, needs to be in the range [0.55, 1.0].
 *
 * @return Tensor of shape `[B, C, Or, H, W]`
 */
torch::Tensor
fractional_erosion(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha);

/**
 * @brief M2 left invariant erosion
 *
 * @param input Tensor of shape `[B, C, Or, H, W]`
 * @param metric_params Tensor of shape `[C, 3, 3]`
 * @param kernel_sizes Specify the sizes of the grid to sample as `{kOr,kH,kW}`
 * @param alpha Alpha parameter, needs to be in the range [0.55, 1.0].
 *
 * @return Tensor of shape `[B, C, Or, H, W]`
 */
torch::Tensor
fractional_erosion_nondiag(
    const torch::Tensor& input,
    const torch::Tensor& metric_params,
    const torch::IntArrayRef kernel_sizes,
    const double alpha);

/**
 * @brief Linear combination of M2 data (Forward).
 *
 * @param input Tensor of shape `[B, Cin, Or, H, W]`
 * @param weight Tensor of shape `[Cin, Cout]`
 *
 * @return Tensor of shape `[B, Cout, Or, H, W]`
 */
torch::Tensor
linear_fw(const torch::Tensor& input, const torch::Tensor& weight);

/**
 * @brief Linear combination of M2 data (Backward)
 *
 * @param grad Tensor of shape `[B, Cout, Or, H, W]`
 * @param input Tensor of shape `[B, Cin, Or, H, W]`
 * @param weight Tensor of shape `[Cin, Cout]`
 *
 * @return Tensors of shape `[B, Cin, Or, H, W]` and `[Cin, Cout]`
 */
std::tuple<torch::Tensor, torch::Tensor>
linear_bw(
    const torch::Tensor& grad,
    const torch::Tensor& input,
    const torch::Tensor& weight);

/**
 * @brief Linear combination of M2 data
 *
 * @param input Tensor of shape `[B, Cin, Or, H, W]`
 * @param weight Tensor of shape `[Cin, Cout]`
 *
 * @return Tensor of shape `[B, Cout, Or, H, W]`
 */
torch::Tensor
linear(const torch::Tensor& input, const torch::Tensor& weight);

/**
 *
 * @brief Linear combination of M2 data (Function).
 *
 */
class Linear : public Function<Linear>
{
public:
    static tensor_list
    forward(AutogradContext* ctx, torch::Tensor input, torch::Tensor weight);

    static tensor_list backward(AutogradContext* ctx, tensor_list grads);
};

} // namespace m2
} // namespace lietorch

#endif