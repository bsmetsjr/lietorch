#pragma once
#ifndef LIETORCH_PARALLEL_H
#define LIETORCH_PARALLEL_H

/** @file cpu/parallel.h
 *
 *  @brief This header includes a for_each implementation that selects the best
 * parallelism mode available on the platform. This parallelized for_each loop
 * is what most of our CPU operations use to take advantage of a multi-core
 * system.
 *
 */
#include <algorithm>
#include <future>

#if _MSC_VER >= 1920 || __GNUC__ >= 9
#include <execution>
#endif

namespace lietorch
{

/**
 *  @brief Available parallelism modes.
 */
enum _parallelism_mode_t
{
    single_thread = 1, // no parallelism, algos are executed in the calling thread.
    std_execution_par_unseq = 2, // C++17 Standard Library parallelism (requires
                                 // `#include <execution>`)
    std_async_divide_and_conquer = 3 // C++11 async divide-and-conquer
};

/**
 *
 *  @return Returns the selected parallelism mode which is selected at
 * compile-time based on the available platform.
 */
inline constexpr _parallelism_mode_t
_parallelism_mode()
{
#if _MSC_VER >= 1920 || __GNUC__ >= 9
    return _parallelism_mode_t::std_execution_par_unseq;
#elif __cplusplus >= 201103L
    return _parallelism_mode_t::std_async_divide_and_conquer;
#else
    return _parallelism_mode_t::single_thread;
#endif
}


/**
 * @brief Divide and conquer style for_each loop that uses std::async to split
 * off work to (potentially) different threads.
 *
 * @param first
 * @param last
 * @param f Function that is callable with dereferenced iterator.
 */
template <class _InputIterator, class _Function>
inline void
for_each_async_dac(_InputIterator first, _InputIterator last, _Function f)
{
    const ptrdiff_t length = last - first;
    if (!length) return;

    if (length == 1)
    {
        f(*first);
        return;
    }

    const _InputIterator mid = first + (length / 2);
    std::future<void> bgtask = std::async(
        &for_each_async_dac<_InputIterator, _Function>, first, mid, f);

    try
    {
        for_each_async_dac(mid, last, f);
    }
    catch (...)
    {
        bgtask.wait();
        throw;
    }
    bgtask.get();
}




/**
 *  @brief For-each loop that select its type of execution based on what type of
 * parallelism support is available on the platform. On a modern (i.e. C++17
 * compliant) platform it will use the std::execution::par_unseq execution
 * policy of the standard library. The next fallback is a divide-and-conquer
 * approach using std::async which should be available for all C++11 platforms.
 * Finally it will fall back on single threading.
 *
 *  @param first
 *  @param last
 *  @param f Function that takes a dereferenced iterator.
 *
 */
template <class _InputIterator, class _Function>
inline void
for_each(_InputIterator first, _InputIterator last, _Function f)
{

    if constexpr (_parallelism_mode() == _parallelism_mode_t::single_thread)
    {
        std::for_each(first, last, f);
    }
    else if (_parallelism_mode() == _parallelism_mode_t::std_execution_par_unseq)
    {
        std::for_each(std::execution::par_unseq, first, last, f);
    }
    else if (_parallelism_mode() == _parallelism_mode_t::std_async_divide_and_conquer)
    {
        for_each_async_dac<_InputIterator, _Function>(first, last, f);
    }
}

} // namespace lietorch

#endif