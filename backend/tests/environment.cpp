
#include "../src/torch_include.h"
#include "gtest/gtest.h"

#ifdef _WIN32
#include <windows.h>
#elif __linux__

#else

#endif

#include "../src/dbg.h"

class LTEnvironment : public ::testing::Environment
{
public:
    ~LTEnvironment() override {}

    void SetUp() override
    {
#ifdef _WIN32
        // Does not always automatically load but it is required for CUDA
        // functionality.
        HINSTANCE hGetProcIDDLL = LoadLibrary("torch_cuda.dll");
        ASSERT_TRUE(hGetProcIDDLL);
#elif __linux__

#else

#endif
    }

    void TearDown() override {}
};

auto const lt_env = ::testing::AddGlobalTestEnvironment(new LTEnvironment);