#include "../src/torch_include.h"
#include "gtest/gtest.h"

#ifdef _WIN32
#include <windows.h>
#undef max
#undef min
#endif

#include "../src/dbg.h"
#include <filesystem>

#include "../src/cpu/temperate_cpu.h"

TEST(TEMPERATE, mul_lut_u8)
{
    constexpr auto m = lietorch::temperate::mul_lut_u8;

    EXPECT_EQ(m(0, 1), 0);
    EXPECT_EQ(m(0, 255), 0);
    EXPECT_EQ(m(32, 193), m(193, 32));
    EXPECT_EQ(m(128, 18), 18);
    EXPECT_EQ(m(1, 255), 128);
    EXPECT_EQ(m(255, 255), 255);

    const auto options = torch::TensorOptions().dtype(torch::kU8);
}