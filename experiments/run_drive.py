#!python
"""
    Run DRIVE retinal vessel segmentation experiments.
"""
import sys, os, time

# expecting to find lietorch package in parent directory
sys.path.append("..")
sys.path.append(".")

import torch
import lietorch
import sty
from mlflow import (
    active_run,
    set_experiment,
    log_metric,
    log_param,
    log_artifact,
    set_tag,
)
from mlflow.pytorch import log_model
from tqdm import tqdm
import sklearn.metrics
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
import tifffile
from pathlib import Path
from PIL import Image
import numpy as np

# Data directories, the first existing directory is used
DATA_PATHS = ["datasets/DRIVE"]
IN_CHANNELS = 3

# Training parameters

# 6 layer networks
# MODEL = lietorch.models.drive.CDEPdeNN6
MODEL = lietorch.models.drive.FixedLiftCDEPdeNN6
EPOCHS = 60
LR = 0.01
LR_GAMMA = 0.95

# 12 layer networks
# MODEL = lietorch.models.drive.CDEPdeNN12
# EPOCHS = 80
# LR = 0.01
# LR_GAMMA = 0.96

BATCH_SIZE = 8
PATCH_SHAPE = [64, 64]
PATCH_OVERLAP = [16, 16]
L2_LOSS_MULTIPLIER = 0.005


def get_datasets(data_dir, patch_shape=None, patch_overlap=None):
    """
        Load and pre-process DRIVE images (x) and segmentation maps (y).
    """
    path_x = Path(data_dir) / "training" / "images"
    path_y = Path(data_dir) / "training" / "1st_manual"

    x = []
    y = []

    for p in sorted(path_x.glob("*.tif")):
        image = tifffile.imread(p)
        x.append(torch.tensor(image, dtype=torch.float32) / 255.0)

    for p in sorted(path_y.glob("*.gif")):
        image = np.array(Image.open(p))
        y.append(torch.tensor(image, dtype=torch.float32) / 255.0)

    x = torch.stack(x)
    y = torch.stack(y)

    # Split training data into patches if requested
    if patch_shape != None:
        # first calculate anchor (corner) point for the individual patches
        h, w = x.shape[1:3]
        patch_h, patch_w = patch_shape
        overlap_h, overlap_w = patch_overlap

        anchors_h = list(range(0, h, patch_h - overlap_h))

        if anchors_h[-1] >= h - overlap_h:
            anchors_h = anchors_h[:-1]
        anchors_h[-1] = h - patch_h

        anchors_w = list(range(0, w, patch_w - overlap_w))

        if anchors_w[-1] >= w - overlap_w:
            anchors_w = anchors_w[:-1]
        anchors_w[-1] = w - patch_w

        anchors = np.reshape(
            np.stack(np.meshgrid(anchors_h, anchors_w), axis=-1), (-1, 2)
        )

        x_patches = []
        y_patches = []

        for i in range(x.shape[0]):
            for j, a in enumerate(anchors):
                x_patch = x[i, a[0] : a[0] + patch_h, a[1] : a[1] + patch_w, :]
                y_patch = y[i, a[0] : a[0] + patch_h, a[1] : a[1] + patch_w]
                # if (y_patch == 0.0).all():
                #    continue
                x_patches.append(x_patch)
                y_patches.append(y_patch)

        x = torch.stack(x_patches)
        y = torch.stack(y_patches)

    # permute dimensions to [batch, channel, height, width]
    x = x.permute(0, 3, 1, 2)
    # convert to long
    y = y.long()

    # Split into training and testing data
    # n_test = x.shape[0] // 5
    # x_train, x_test = torch.split(x, [x.shape[0] - n_test, n_test])
    # y_train, y_test = torch.split(y, [y.shape[0] - n_test, n_test])

    train_set = torch.utils.data.TensorDataset(x, y)

    path_x_test = Path(data_dir) / "test" / "images"
    path_y_test = Path(data_dir) / "test" / "1st_manual"

    x_test = []
    y_test = []

    for p in sorted(path_x_test.glob("*.tif")):
        image = tifffile.imread(p)
        x_test.append(torch.tensor(image, dtype=torch.float32) / 255.0)

    for p in sorted(path_y_test.glob("*.gif")):
        image = np.array(Image.open(p))
        y_test.append(torch.tensor(image, dtype=torch.float32) / 255.0)

    x_test = torch.stack(x_test)
    y_test = torch.stack(y_test)

    x_test = x_test.permute(0, 3, 1, 2)
    y_test = y_test.long()

    test_set = torch.utils.data.TensorDataset(x_test, y_test)

    return train_set, test_set


def train(model, device, train_loader, optimizer, total_params: int):
    """
        Train one epoch
    """
    model.train()
    for batch_idx, (x, y) in tqdm(
        enumerate(train_loader),
        total=len(train_loader),
        desc="Training",
        dynamic_ncols=True,
        unit="batch",
    ):
        x, y = x.to(device), y.to(device)
        optimizer.zero_grad()
        output = model(x)
        batch_loss = loss(model, device, output, y, total_params)
        log_metric("train_loss", float(batch_loss.cpu().item()))
        batch_loss.backward()
        optimizer.step()


def test(model, device, test_loader, total_params: int):
    """
        Evaluate the model
    """
    model.eval()
    test_loss = []
    acc_score = []
    auc_score = []
    dice_score = []

    start = time.perf_counter()

    with torch.no_grad():
        for x, y in test_loader:
            x, y = x.to(device), y.to(device)

            output = model(x)

            test_loss.append(loss(model, device, output, y, total_params).item())
            y = y.cpu().view(-1)
            prediction = output.round().cpu().view(-1)
            auc_score.append(sklearn.metrics.roc_auc_score(y, prediction))
            acc_score.append(sklearn.metrics.accuracy_score(y, prediction))
            dice_score.append(
                float((2 * torch.sum(prediction * y)).item())
                / float((prediction.sum() + y.sum()).item())
            )

    stop = time.perf_counter()
    process_time = float(stop - start)

    test_loss = np.mean(test_loss)
    auc_score = np.mean(auc_score)
    acc_score = np.mean(acc_score)
    dice_score = np.mean(dice_score)

    # Log metrics to MLFlow
    log_metric("process_time", float(process_time))
    log_metric("test_loss", float(test_loss))
    log_metric("acc_score", float(acc_score))
    log_metric("auc_score", float(auc_score))
    log_metric("dice_score", float(dice_score))

    # Print summary
    print(
        f"Test: time = "
        + f"{float(process_time):.1f}s"
        + f"\t loss = "
        + f"{float(test_loss):.4f}"
        # + f"\t acc = "
        # + f"{float(acc_score):.4f}"
        # + f"\t auc = "
        # + f"{float(auc_score):.4f}"
        + f"\t dice = "
        + sty.fg.li_green
        + f"{float(dice_score):.4f}"
        + sty.rs.all
    )

def showoff(model, device, test_loader):
    model.eval()

    with torch.no_grad():
        for x, y in test_loader:
            x, y = x.to(device), y.to(device)
            o = model(x)

            x = x.cpu().detach().numpy()
            y = y.cpu().detach().numpy()
            o = o.cpu().detach().numpy()

            x = x*255
            y = y*255
            o = o*255

            x = x.astype('uint8')
            y = y.astype('uint8')
            o = o.astype('uint8')

            x = x.transpose((0, 2, 3, 1))
            x = x[0,:,:,:]
            y = y[0,:,:]
            o = o[0,0,:,:]

            x = Image.fromarray(x, 'RGB')
            y = Image.fromarray(y, 'L')
            o = Image.fromarray(o, 'L')

            x.show()
            y.show()
            o.show()

def loss(model, device, output, y, total_params: int):
    """
        Calculate loss including regularization loss.
    """
    # n = y.numel()
    # npos = np.max([n // 100, int(y.sum().item())])
    # weight = torch.tensor((n / (n - npos), n / npos), device=device).float()

    # data_loss = F.cross_entropy(output, y, weight=weight)
    data_loss = lietorch.nn.functional.dice_loss(output, y)

    l2_loss = torch.tensor(0.0, device=device)
    for p in model.parameters():
        l2_loss += p.pow(2).sum()

    l2_loss = l2_loss / total_params

    return data_loss + torch.tensor(L2_LOSS_MULTIPLIER, device=device) * l2_loss

def user_prompt(question):
    from distutils.util import strtobool
    while True:
        user_input = input(question + " [y/n]: ")
        try:
            return bool(strtobool(user_input))
        except ValueError:
            print("Please use y/yes/t/true/on/1 or n/no/f/false/off/0.\n")

if __name__ == "__main__":

    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Check which of the given data directories exist
    if sum([os.path.exists(p) for p in DATA_PATHS]) == 0:  # stop if none exist
        raise FileNotFoundError(
            f"None of the given data directories {DATA_PATHS} exist."
        )

    data_path = next(filter(lambda p: os.path.exists(p), DATA_PATHS))

    # Setup MLFlow tracking
    set_experiment("DRIVE20 DPFR")
    set_tag("MODEL", f"{MODEL.__module__}.{MODEL.__name__}")
    set_tag("LIETORCH_VERSION", lietorch.__version__)
    set_tag("PYTORCH_VERSION", torch.__version__)

    log_param("BATCH_SIZE", BATCH_SIZE)
    log_param("EPOCHS", EPOCHS)
    log_param("LR", LR)
    log_param("LR_GAMMA", LR_GAMMA)
    log_param("PATCH_SHAPE", PATCH_SHAPE)
    log_param("PATCH_OVERLAP", PATCH_OVERLAP)

    # Log this script and the model source file
    log_artifact(__file__)
    log_artifact(sys.modules[MODEL.__module__].__file__)

    # Log conda environment
    os.system("conda env export > environment.yml")
    # os.system("conda env export --from-history > requested_environment.yml")
    log_artifact("environment.yml")
    # log_artifact("requested_environment.yml")

    # get dataset
    train_set, test_set = get_datasets(data_path, PATCH_SHAPE, PATCH_OVERLAP)
    train_loader = torch.utils.data.DataLoader(
        train_set, batch_size=BATCH_SIZE, shuffle=True
    )
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=False)

    print(f"LieTorch version: {lietorch.__version__}")
    print(f"LieTorch location: {lietorch.__file__}")
    print(f"PyTorch version: {torch.__version__}")
    print(f"PyTorch location: {torch.__file__}")
    print(f"DRIVE Experiment (run_id: {active_run().info.run_id})")
    print(f'Using data directory "{data_path}"')
    cc = torch.cuda.get_device_capability(device)
    print(
        f'Using device "{device}": {torch.cuda.get_device_name(device)} (compute capability {cc[0]}.{cc[1]})'
    )

    # instanciate model
    model = MODEL(IN_CHANNELS).to(device)
    # optimizer = torch.optim.SGD(model.parameters(), lr=LR, momentum=0.5)
    optimizer = torch.optim.Adam(model.parameters(), lr=LR)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, LR_GAMMA)

    total_params = sum(p.numel() for p in model.parameters(recurse=True))
    log_param("TOTAL_TRAINABLE_PARAMETERS", total_params)

    print(
        f"Model: {MODEL.__module__}."
        + sty.fg.li_green
        + f"{MODEL.__name__}"
        + sty.rs.all
        + f" with {total_params} parameters"
    )

    try:
        for epoch in range(1, EPOCHS + 1):
            print(
                sty.fg.white
                + sty.bg.li_blue
                + sty.ef.b
                + f"Epoch {epoch}/{EPOCHS}:"
                + sty.rs.all
            )
            train(model, device, train_loader, optimizer, total_params)
            test(model, device, test_loader, total_params)
            scheduler.step()

    except KeyboardInterrupt:
        print('Interrupted')

    except SystemExit:
        os._exit(0)
        
    try:
        if(user_prompt("Do you want to log the model?") == 1):
            print("Logging model...")
            log_model(model, "models")
            
        if(user_prompt("Do you want show off the model?") == 1):
            print("Showing off..")
            showoff(model, device, test_loader)

    except KeyboardInterrupt:
        print('Interrupted')

    except SystemExit:
        os._exit(0)