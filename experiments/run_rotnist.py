"""
    Run RotNIST digit classification.
"""
import sys, os, time

# expecting to find lietorch package in parent directory
sys.path.append("..")
sys.path.append(".")

import gzip
import torch
import torch.nn.functional as F
import lietorch
from mlflow import (
    active_run,
    set_experiment,
    log_metric,
    log_param,
    log_artifact,
    set_tag,
)
from mlflow.pytorch import log_model
from tqdm import tqdm
from pathlib import Path
import sty
import numpy as np

# Data directories, the first existing directory is used
DATA_PATHS = ["datasets\RotNIST"]

# Model
MODEL = lietorch.models.rotnist.GroupM2Classifier4
EPOCHS = 60
LR = 0.05
LR_GAMMA = 0.96
BATCH_SIZE = 256
L2_LOSS_MULTIPLIER = 0.005


def test(model, device, test_loader, total_params: int):
    """
        Evaluate the model
    """
    model.eval()
    test_loss = []
    acc_score = []

    start = time.perf_counter()

    with torch.no_grad():
        for x, y in test_loader:
            x, y = x.to(device), y.to(device)
            output = model(x)

            test_loss.append(loss(model, device, output, y, total_params).item())
            y = y.cpu().view(-1)
            _, prediction = torch.max(output.cpu(), dim=1)
            acc_score.append((y == prediction).sum().item() / float(y.numel()))

    stop = time.perf_counter()
    process_time = float(stop - start)

    test_loss = np.mean(test_loss)
    acc_score = np.mean(acc_score)

    # Log metrics to MLFlow
    log_metric("process_time", float(process_time))
    log_metric("test_loss", float(test_loss))
    log_metric("acc_score", float(acc_score))

    # Print summary
    print(
        f"Test: time = "
        + f"{float(process_time):.2f}s"
        + f"\t loss = "
        + f"{float(test_loss):.4f}"
        + f"\t acc = "
        + f"{float(acc_score):.4f}"
        + sty.rs.all
    )


def train(model, device, train_loader, optimizer, total_params: int):
    """
        Train one epoch
    """
    model.train()
    for batch_idx, (x, y) in tqdm(
        enumerate(train_loader),
        total=len(train_loader),
        desc="Training",
        dynamic_ncols=True,
        unit="batch",
    ):
        x, y = x.to(device), y.to(device)
        optimizer.zero_grad()
        output = model(x)
        batch_loss = loss(model, device, output, y, total_params)
        log_metric("train_loss", float(batch_loss.cpu().item()))
        batch_loss.backward()
        optimizer.step()


def loss(model, device, output, y, total_params: int):
    """

    """
    data_loss = F.cross_entropy(output, y)

    l2_loss = torch.tensor(0.0, device=device)
    for p in model.parameters():
        l2_loss += p.pow(2).sum()

    l2_loss = l2_loss / total_params

    return data_loss + torch.tensor(L2_LOSS_MULTIPLIER, device=device) * l2_loss

def user_prompt(question):
    from distutils.util import strtobool
    while True:
        user_input = input(question + " [y/n]: ")
        try:
            return bool(strtobool(user_input))
        except ValueError:
            print("Please use y/yes/t/true/on/1 or n/no/f/false/off/0.\n")

if __name__ == "__main__":

    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Check which of the given data directories exist
    if sum([os.path.exists(p) for p in DATA_PATHS]) == 0:  # stop if none exist
        raise FileNotFoundError(
            f"None of the given data directories {DATA_PATHS} exist."
        )

    data_path = next(filter(lambda p: os.path.exists(p), DATA_PATHS))

    # Setup MLFlow tracking
    set_experiment("RotNIST")
    set_tag("MODEL", f"{MODEL.__module__}.{MODEL.__name__}")
    set_tag("LIETORCH_VERSION", lietorch.__version__)
    set_tag("PYTORCH_VERSION", torch.__version__)

    log_param("BATCH_SIZE", BATCH_SIZE)
    log_param("EPOCHS", EPOCHS)
    log_param("LR", LR)
    log_param("LR_GAMMA", LR_GAMMA)

    # Log this script and the model source file
    log_artifact(__file__)
    log_artifact(sys.modules[MODEL.__module__].__file__)

    (
        train_img,
        train_lbl,
        test_img,
        test_lbl,
    ) = lietorch.models.rotnist.load_rotnist_data_from_path(data_path)

    train_img = torch.unsqueeze(train_img.to(torch.float32), 1) / 255.0
    test_img = torch.unsqueeze(test_img.to(torch.float32), 1) / 255.0
    train_lbl = train_lbl.to(torch.long)
    test_lbl = test_lbl.to(torch.long)

    train_set = torch.utils.data.TensorDataset(train_img, train_lbl)
    test_set = torch.utils.data.TensorDataset(test_img, test_lbl)

    train_loader = torch.utils.data.DataLoader(
        train_set, batch_size=BATCH_SIZE, shuffle=True
    )
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=512, shuffle=False)

    print(f"LieTorch v{lietorch.__version__}, PyTorch v{torch.__version__}")
    print(f"DRIVE Experiment (run_id: {active_run().info.run_id})")
    print(f'Using data directory "{data_path}"')
    cc = torch.cuda.get_device_capability(device)
    print(
        f'Using device "{device}": {torch.cuda.get_device_name(device)} (compute capability {cc[0]}.{cc[1]})'
    )

    # instanciate model
    model = MODEL().to(device)
    # optimizer = torch.optim.SGD(model.parameters(), lr=LR, momentum=0.5)
    optimizer = torch.optim.Adam(model.parameters(), lr=LR)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, LR_GAMMA)

    total_params = sum(p.numel() for p in model.parameters(recurse=True))
    log_param("TOTAL_TRAINABLE_PARAMETERS", total_params)

    print(
        f"Model: {MODEL.__module__}."
        + sty.fg.li_green
        + f"{MODEL.__name__}"
        + sty.rs.all
        + f" with {total_params} parameters"
    )

    try:
        for epoch in range(1, EPOCHS + 1):
            print(
                sty.fg.white
                + sty.bg.li_blue
                + sty.ef.b
                + f"Epoch {epoch}/{EPOCHS}:"
                + sty.rs.all
            )
            train(model, device, train_loader, optimizer, total_params)
            test(model, device, test_loader, total_params)
            scheduler.step()

    except KeyboardInterrupt:
        print('Interrupted')

    except SystemExit:
        os._exit(0)

    try:
        if(user_prompt("Do you want to log the model?") == 1):
            print("Logging model...")
            log_model(model, "models")

    except KeyboardInterrupt:
        print('Interrupted')

    except SystemExit:
        os._exit(0)

